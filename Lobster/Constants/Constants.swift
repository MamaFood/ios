//
//  Constants.swift
//  Lobster
//
//  Created by JustDoIt on 07.08.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import UIKit

// MARK: - kConstraints
let kAccessToken = "access_token"
let kAnimateTime = 0.35

// MARK: - General
let appModel = AppModel.sharedInstance
let appDelegate = UIApplication.shared.delegate as? AppDelegate
let window = appDelegate?.window

// MARK: - Storyboards
let loginStoryboard = UIStoryboard.init(name: "Login", bundle: Bundle.main)
let mainCollectionStoryboard = UIStoryboard.init(name: "MainCollection", bundle: Bundle.main)
let restaurantStoryboard = UIStoryboard.init(name: "Restaurant", bundle: Bundle.main)
let checkoutStoryboard = UIStoryboard.init(name: "Checkout", bundle: Bundle.main)

// MARK: - Features
func localize(_ string: String!) -> String {
    return LocalizeHelper.localizedString(forKey: string)
}

func addDishToCart(_ dish: Dish) {
//    Navigator.shared.cartViewController.addDish(dish: dish)
}

func removeDishFromCart(_ index: Int) {
//    Navigator.shared.cartViewController.removeDish(index: index)
}

// MARK: – HUD
func showHUD() {
    let bounds = UIScreen.main.bounds
    let grayView = UIView(frame: bounds)
    grayView.tag = 90
    grayView.backgroundColor = .black
    grayView.alpha = 0.5

    let activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
    activityIndicator.frame = bounds
    activityIndicator.startAnimating()

    grayView.addSubview(activityIndicator)
    window?.rootViewController?.view.addSubview(grayView)
}

func hideHUD() {
    guard let view = window?.rootViewController?.view.subviews.last else { return }
    if (view.tag == 90) {
        view.removeFromSuperview()
    }
}

struct SearchPlacesModel {
    let addres: String
    let additionalAddres: String!
}
