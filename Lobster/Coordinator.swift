//
//  Navigator.swift
//  Lobster
//
//  Created by JustDoIt on 20.09.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import UIKit

enum ViewControllerIdentifier {
    case tabBarController,
    launchScreenViewController,
    signInViewController,
    signUpViewController,
    registerImageSetViewController,
    continueRegisterViewController,
    scannerViewController,
    restaurantCategoriesViewController,
    restaurantsCollectionViewController,
    restaurantViewController,
    menuViewController,
    dishesViewController,
    cartViewController,
    searchViewController,
    mapViewController
}

class Coordinator {

    static let shared = Coordinator()

    var tabBarController: UITabBarController!
    var rootViewController: UIViewController! {
        didSet {
            window?.rootViewController = rootViewController
        }
    }

    private init() {
        tabBarController = UITabBarController()
        var viewControllersList: [UIViewController] = [UIViewController]()

        if let homeViewController = Coordinator.instantiateViewController(with: .restaurantCategoriesViewController) {
            homeViewController.tabBarItem = UITabBarItem(title: "", image: UIImage(named: "home-tab-icon-3"), tag: 0)
            viewControllersList.append(homeViewController)
        }

        if let searchViewController = Coordinator.instantiateViewController(with: .searchViewController) {
            searchViewController.tabBarItem = UITabBarItem(title: "", image: UIImage(named: "search-tab-icon-3"), tag: 2)
            viewControllersList.append(searchViewController)
        }

        if let restaurantViewController = Coordinator.instantiateViewController(with: .restaurantViewController) as? RestaurantViewController {
            restaurantViewController.restaurant = TempRestaurant.sharedInstance.getRestaurant()
            restaurantViewController.tabBarItem = UITabBarItem(title: "", image: UIImage(named: "card-tab-icon-3"), tag: 1)
            viewControllersList.append(restaurantViewController)
        }

        if let cartViewController = Coordinator.instantiateViewController(with: .cartViewController) {
            cartViewController.tabBarItem = UITabBarItem(title: "", image: UIImage(named: "check-tab-icon-3"), tag: 3)
            viewControllersList.append(cartViewController)
        }

        let viewControllers = viewControllersList.map { (viewController) -> UIViewController in
            let navigationController = UINavigationController(rootViewController: viewController)
//            if viewController is CategoriesViewController {
                navigationController.setNavigationBarHidden(true, animated: false)
//            }
            return navigationController
        }
        tabBarController.setViewControllers(viewControllers, animated: true)
        setRootViewControllerWith(viewController: tabBarController, modalTransitionStyle: nil)
    }

    func setRootViewControllerWith(identifier: ViewControllerIdentifier, modalTransitionStyle: UIModalTransitionStyle?) {
        if let viewController = Coordinator.instantiateViewController(with: identifier) {
            viewController.modalTransitionStyle = modalTransitionStyle ?? .coverVertical
            self.rootViewController = viewController
        }
    }

    func setRootViewControllerWith(viewController: UIViewController, modalTransitionStyle: UIModalTransitionStyle?) {
        viewController.modalTransitionStyle = modalTransitionStyle ?? .coverVertical
        self.rootViewController = viewController
    }
    
    func presentViewController(identifier: ViewControllerIdentifier, completion: (() -> Void)?) {
        if let viewController = Coordinator.instantiateViewController(with: identifier) {
            if self.rootViewController is UITabBarController {
                if let rootViewController = self.rootViewController as? UITabBarController {
                    if let selectedViewController = rootViewController.selectedViewController as? UINavigationController {
                        // selectedViewController is always UINavigationController
                        if viewController is UINavigationController {
                            selectedViewController.present(viewController, animated: true, completion: completion)
                        } else {
                            selectedViewController.pushViewController(viewController, animated: true)
                        }
                    }
                }
            } else {
                if let modalVC = self.rootViewController.presentedViewController {
                    modalVC.present(viewController, animated: true, completion: completion)
                } else {
                    self.rootViewController.present(viewController, animated: true, completion: completion)
                }
            }
        }
    }

    func presentViewController(viewController: UIViewController, modalTransitionStyle: UIModalTransitionStyle?, completion: (() -> Void)?) {
        viewController.modalTransitionStyle = modalTransitionStyle ?? .coverVertical
        if self.rootViewController is UITabBarController {
            if let rootViewController = self.rootViewController as? UITabBarController {
                if let selectedViewController = rootViewController.selectedViewController as? UINavigationController {
                    // selectedViewController is always UINavigationController
                    if viewController is UINavigationController {
                        selectedViewController.present(viewController, animated: true, completion: completion)
                    } else {
                        selectedViewController.pushViewController(viewController, animated: true)
                    }
                }
            }
        } else {
            if let modalVC = self.rootViewController.presentedViewController {
                modalVC.present(viewController, animated: true, completion: completion)
            } else {
                self.rootViewController.present(viewController, animated: true, completion: completion)
            }
        }
    }

    static func instantiateViewController(with identifier: ViewControllerIdentifier) -> UIViewController! {
        var viewControllerIdentifier: String!
        var storyboard: UIStoryboard!

        switch identifier {
        case .launchScreenViewController:
            viewControllerIdentifier = "LaunchScreenViewController"
            storyboard = loginStoryboard
        case .signInViewController:
            viewControllerIdentifier = "LoginAndRegisterViewController"
            storyboard = loginStoryboard
        case .signUpViewController:
            viewControllerIdentifier = "LoginAndRegisterViewController"
            storyboard = loginStoryboard
        case .registerImageSetViewController:
            viewControllerIdentifier = "RegisterImageSetViewController"
            storyboard = loginStoryboard
        case .continueRegisterViewController:
            viewControllerIdentifier = "ContinueRegisterViewController"
            storyboard = loginStoryboard
        case .searchViewController:
            viewControllerIdentifier = "SearchViewController"
            storyboard = mainCollectionStoryboard
        case .mapViewController:
            viewControllerIdentifier = "SearchMapViewController"
            storyboard = mainCollectionStoryboard
        case .restaurantCategoriesViewController:
            viewControllerIdentifier = "CategoriesViewController"
            storyboard = mainCollectionStoryboard
        case .restaurantsCollectionViewController:
            viewControllerIdentifier = "RestaurantsCollectionViewController"
            storyboard = mainCollectionStoryboard
        case .restaurantViewController:
            viewControllerIdentifier = "RestaurantViewController"
            storyboard = restaurantStoryboard
        case .dishesViewController:
            viewControllerIdentifier = "DishesViewController"
            storyboard = mainCollectionStoryboard
        case .menuViewController:
            viewControllerIdentifier = "UISideMenuNavigationController"
            storyboard = mainCollectionStoryboard
        case .cartViewController:
            viewControllerIdentifier = "CheckListViewController"
            storyboard = checkoutStoryboard
        case .scannerViewController:
            let scannerViewController = ScannerViewController()
            return scannerViewController
        case .tabBarController:
            return Coordinator.shared.tabBarController
        }

        if viewControllerIdentifier != nil || storyboard != nil {
            let viewController = storyboard.instantiateViewController(withIdentifier: viewControllerIdentifier)
            configure(viewController: viewController, identifier: identifier)
            return viewController
        } else {
            return nil
        }
    }

    static func configure(viewController: UIViewController, identifier: ViewControllerIdentifier) {
        switch identifier {
        case .signInViewController:
            if let viewController = viewController as? LoginAndRegisterViewController {
                viewController.changeState(state: .signIn)
            }
        case .signUpViewController:
            if let viewController = viewController as? LoginAndRegisterViewController {
                viewController.changeState(state: .signUp)
            }
        default:
            return
        }
    }

}
