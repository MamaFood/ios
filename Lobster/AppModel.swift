//
//  AppModel.swift
//  Lobster
//
//  Created by Rauf on 24.09.17.
//  Copyright © 2017 RGS. All rights reserved.
//

import UIKit
import GoogleSignIn
import GoogleMaps

class AppModel: NSObject {

    static let sharedInstance = AppModel()

    // States
    var isAuthorized: Bool!

    private override init() {
        super.init()
        GIDSignIn.sharedInstance().clientID = "41588123346-4t2ltdkv3vp81jbot6rk7rdb8i2ehepm.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self
        GMSServices.provideAPIKey("AIzaSyDXpXMoe2jxdjnbbqgJ_m8Tv49szKkErsE")
        isAuthorized = checkIsAuthorized()
    }

    private func checkIsAuthorized() -> Bool {
        if UserDefaults.standard.value(forKey: kAccessToken) != nil {
            return true
        } else {
            return false
        }
    }

}

extension AppModel: GIDSignInDelegate {

    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        guard error != nil, user.serverAuthCode != nil else {
            print(error.localizedDescription)
            return
        }
        print(user.profile.name)
    }

    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {

    }

}
