//
//  NavigationBarView.swift
//  Lobster
//
//  Created by JustDoIt on 23.12.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import UIKit
import GooglePlaces

protocol NavigationBarViewDelegate: class {
    func willHideNavigationBarView()
    func searchCollectionViewDidShow()
    func searchCollectionViewDidHide()
    func mapButtonPressed()
    func searchButtonPressed()
    func menuButtonPressed()
}

class NavigationBarView: UIView {

    private let kPlaceCollectionViewCell = "categoriesCollectionViewPlaceCell"
    let kNavigationViewBarHideHeight: CGFloat = CGFloat(exactly: 55.0)!
    let kNavigationViewBarShowHeight: CGFloat = CGFloat(exactly: 227.0)!

    @IBOutlet weak var navigationBarView: UIView!
    @IBOutlet weak var geoPositionButton: UIButton! {
        didSet {
            if let titleLabel = geoPositionButton.titleLabel {
                titleLabel.numberOfLines = 1
                titleLabel.adjustsFontSizeToFitWidth = true
                titleLabel.lineBreakMode = .byClipping
            }
        }
    }
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var mapButton: UIButton!
    @IBOutlet weak var searchBar: UISearchBar! {
        didSet {
            searchBar.delegate = self
        }
    }
    @IBOutlet weak var searchCollectionView: UICollectionView! {
        didSet {
            searchCollectionView.dataSource = self
            searchCollectionView.delegate = self
            searchCollectionView.register(UINib(nibName: "NavigationBarViewPlaceCell",
                                                bundle: Bundle.main),
                                          forCellWithReuseIdentifier: kPlaceCollectionViewCell)
        }
    }

    weak var delegate: NavigationBarViewDelegate?
    var selectedSearchPlace: SearchPlacesModel!
    var searchPlacesCollection: [SearchPlacesModel]!
    
    class func instanceFromNib() -> NavigationBarView {
        if let navigationBarView = UINib.init(nibName: "NavigationBarView",
                                              bundle: Bundle.main).instantiate(
                                                withOwner: self,
                                                options: nil).first as? NavigationBarView {
            return navigationBarView
        } else {
            return NavigationBarView.init(frame: CGRect.zero)
        }
    }

    private func searchPlaces(text: String, completion: @escaping ([SearchPlacesModel]?, Error?) -> Void) {
        GMSPlacesClient.shared().autocompleteQuery(text, bounds: nil,
                                                   boundsMode: .bias,
                                                   filter: nil) { (response, error) in
                                                    guard response != nil else { return }
                                                    var places: [SearchPlacesModel] = [SearchPlacesModel]()
                                                    for place in response! {
                                                        let full = place.attributedFullText.string
                                                        let primary = place.attributedPrimaryText.string
                                                        let searchPlacesTemp = SearchPlacesModel.init(
                                                            addres: full,
                                                            additionalAddres: primary)
                                                        places.append(searchPlacesTemp)
                                                    }
                                                    if let err = error {
                                                        completion(nil, err)
                                                    } else {
                                                        completion(places, nil)
                                                    }
        }
    }
    
    @IBAction func geoPositionButtonPressed(_ sender: Any) {
        showSearchBar()
    }
    
    @IBAction func mapButtonPressed(_ sender: Any) {
        delegate?.mapButtonPressed()
    }
    
    @IBAction func searchButtonPressed(_ sender: Any) {
        delegate?.searchButtonPressed()
    }
    
    @IBAction func menuButtonPressed(_ sender: Any) {
        delegate?.menuButtonPressed()
    }
}

// MARK: - Manage States
extension NavigationBarView {
    
    func showSearchBar() {
        UIView.animate(withDuration: kAnimateTime) {
            self.searchBar.alpha = 1.0
            self.searchCollectionView.alpha = 1.0
            self.navigationBarView.alpha = 0.0
        }
        navigationBarView.isHidden = true
        searchBar.isHidden = false
        searchCollectionView.isHidden = false
        searchBar.becomeFirstResponder()
        
        if let delegate = searchBar.delegate, let searchBarText = searchBar.text {
            delegate.searchBar!(searchBar, textDidChange: searchBarText)
        }
    }
    
    func hideSearchBar() {
        UIView.animate(withDuration: kAnimateTime) {
            self.searchBar.alpha = 0.0
            self.navigationBarView.alpha = 1.0
        }
        searchBar.isHidden = true
        navigationBarView.isHidden = false
        self.endEditing(true)
        self.searchPlacesCollection = nil
        self.searchCollectionView.reloadData()
        hideSearchCollectionView()
    }
    
    func showSearchCollectionView() {
        self.searchCollectionView.isHidden = false
        self.searchCollectionView.flashScrollIndicators()
        let tempX = self.frame.origin.x
        let tempY = self.frame.origin.y
        let tempWidth = self.frame.size.width
        self.frame = CGRect(x: tempX, y: tempY, width: tempWidth, height: kNavigationViewBarShowHeight)
        if let delegate = self.delegate {
            delegate.searchCollectionViewDidShow()
        }
        UIView.animate(withDuration: kAnimateTime) {
            self.searchCollectionView.alpha = 1.0
            self.layoutIfNeeded()
        }
    }
    
    func hideSearchCollectionView() {
        self.searchCollectionView.isHidden = true
        let tempX = self.frame.origin.x
        let tempY = self.frame.origin.y
        let tempWidth = self.frame.size.width
        self.frame = CGRect(x: tempX, y: tempY, width: tempWidth, height: kNavigationViewBarHideHeight)
        if let delegate = self.delegate {
            delegate.searchCollectionViewDidHide()
        }
        searchBar.text = ""
        searchPlacesCollection = nil
        searchCollectionView.reloadData()
        UIView.animate(withDuration: kAnimateTime) {
            self.searchCollectionView.alpha = 0.0
            self.layoutIfNeeded()
        }
    }
}

extension NavigationBarView:
    UICollectionViewDataSource,
    UICollectionViewDelegate,
    UICollectionViewDelegateFlowLayout,
    UISearchBarDelegate {
    
    func collectionView(_ collectionView: UICollectionView,
                                 numberOfItemsInSection section: Int) -> Int {
        return searchPlacesCollection == nil ? 0 : searchPlacesCollection.count
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == searchCollectionView {
            if let navBarPlaceCollectionViewCell = collectionView.dequeueReusableCell(
                withReuseIdentifier: kPlaceCollectionViewCell,
                for: indexPath) as? NavigationBarViewPlaceCell {
            navBarPlaceCollectionViewCell.configureWith(place: searchPlacesCollection![indexPath.row])
            return navBarPlaceCollectionViewCell
            }
        }
        return UICollectionViewCell()
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        geoPositionButton.setTitle(searchPlacesCollection[indexPath.row].addres, for: .normal)
        selectedSearchPlace = searchPlacesCollection[indexPath.row]
        hideSearchBar()
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let bounds = UIScreen.main.bounds
        return CGSize(width: bounds.width, height: 76)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            self.searchPlacesCollection = nil
            self.searchCollectionView.reloadData()
            self.hideSearchCollectionView()
        } else {
            searchPlaces(text: searchText) { (places, _) -> Void in
                self.searchPlacesCollection = places
                self.searchCollectionView.reloadData()
                self.showSearchCollectionView()
            }
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        hideSearchBar()
    }

}
