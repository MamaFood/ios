//
//  AppDelegate.swift
//  Lobster
//
//  Created by Rauf on 24.09.17.
//  Copyright © 2017 RGS. All rights reserved.
//

import UIKit
import GoogleSignIn
import UserNotifications
import GooglePlaces
import SideMenu

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        self.window = UIWindow.init(frame: UIScreen.main.bounds)
        self.window?.makeKeyAndVisible()

        // SideMenu Configure
        SideMenuManager.default.menuFadeStatusBar = false

        // Google Place
        GMSPlacesClient.provideAPIKey("AIzaSyA2h-j-YtsH7trSW2Tq-947JmqegBdk2To")

        let appModel = AppModel.sharedInstance
//        if !appModel.isAuthorized {
//            Navigator.shared.setRootViewControllerWith(identifier: .launchScreenViewController, modalTransitionStyle: nil)
//        } else {
            Coordinator.shared.setRootViewControllerWith(identifier: .tabBarController, modalTransitionStyle: nil)
//        }
//        self.sendNotification()
        return true
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey: Any] = [:]) -> Bool {
        return GIDSignIn.sharedInstance().handle(url, sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplication.OpenURLOptionsKey.annotation])
    }

    func sendNotification() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge], completionHandler: {_, _ in

        })
        let content = UNMutableNotificationContent()
        content.title = "\"Столовая LaZania\""
        content.body = "Приглашает Вас на вкусный завтрак!"
        content.badge = 1
        let icon: UNNotificationAttachment
        let urlpath = Bundle.main.path(forResource: "onpu", ofType: "jpg")
        let imageURL = URL(fileURLWithPath: urlpath!)
        do {
            icon = try! UNNotificationAttachment(identifier: "image", url: imageURL, options: nil)
        } catch {
        }
        if icon != nil {
            content.attachments = [icon]
        }
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 2, repeats: false)
        let request = UNNotificationRequest(identifier: "SimplifiedIOSNotification", content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }

}
