//
//  String.swift
//  Lobster
//
//  Created by JustDoIt on 22.08.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import Foundation
import PhoneNumberKit

extension String {

    func isValidEmail() -> Bool {
        let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
    }

    func isValidPassword() -> Bool {
        // Password should be 8 chars or more
        let minCharCount = 8
        return count >= minCharCount
    }

    func isValidName() -> Bool {
        let allovedCharacterSet = CharacterSet(charactersIn: "'- ")
        let clearedString = self.components(separatedBy: allovedCharacterSet).joined()
        let otherCharacterSet = CharacterSet.letters.inverted
        if clearedString.rangeOfCharacter(from: otherCharacterSet,
                                          options: .caseInsensitive,
                                          range: nil) != nil {
            return false
        }
        if count > 0 && count <= 50 {
            return true
        }
        return false
    }

}
