//
//  UIColor.swift
//  Lobster
//
//  Created by JustDoIt on 21.10.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import UIKit

extension UIColor {
    static let redDefault = UIColor(red: 1.00, green: 0.21, blue: 0.26, alpha: 1.0)
    static let blueDefault = UIColor(red:0.31, green:0.51, blue:1.00, alpha:1.0)
    static var grayDefault = UIColor(red: 0.94, green: 0.94, blue: 0.94, alpha: 1.0)
    static let greenDefault = UIColor(red:0.25, green:0.80, blue:0.40, alpha:1.0)
    static let orangeDefault = UIColor(red:1.00, green:0.69, blue:0.21, alpha:1.0)
    static let carrotDefault = UIColor(red:1.00, green:0.45, blue:0.31, alpha:1.0)
}
