//
//  UIButton.swift
//  Lobster
//
//  Created by JustDoIt on 23.01.2019.
//  Copyright © 2019 RGS. All rights reserved.
//

import UIKit

extension UIButton {
    
    func enableButton(color: UIColor = UIColor.redDefault) {
        self.layer.borderColor = color.cgColor
        self.setTitleColor(color, for: .normal)
    }
    
    func disableButton(color: UIColor = UIColor.grayDefault) {
        self.layer.borderColor = color.cgColor
        self.setTitleColor(UIColor.black, for: .normal)
    }
    
}
