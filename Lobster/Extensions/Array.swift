//
//  Array.swift
//  Lobster
//
//  Created by JustDoIt on 25.11.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import Foundation

extension Array where Element: Category {
    func contains(_ category: Category) -> Bool {
        for index in self {
            if category.id == index.id { return true }
        }
        return false
    }
}
