//
//  UIView.swift
//  Lobster
//
//  Created by JustDoIt on 22.12.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import UIKit

extension UIView {
    
    func border(color: UIColor = UIColor.grayDefault,
                width: CGFloat = 1.0) {
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = width
    }

    func cornerRadius(radius: CGFloat = 8.0) {
        self.clipsToBounds = true
        self.layer.cornerRadius = radius
    }

    func shadow(opacity: Float = 0.3,
                offset: CGSize = CGSize(width: 0, height: 1),
                radius: CGFloat = 3) {
        self.clipsToBounds = false
        self.layer.shadowOpacity = opacity
        self.layer.shadowOffset = offset
        self.layer.shadowRadius = radius
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds,
                                             cornerRadius: self.layer.cornerRadius).cgPath
    }
    
}
