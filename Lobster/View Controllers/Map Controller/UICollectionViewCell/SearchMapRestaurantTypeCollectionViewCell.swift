//
//  SearchMapRestaurantTypeCollectionViewCell.swift
//  Lobster
//
//  Created by JustDoIt on 09.02.2019.
//  Copyright © 2019 RGS. All rights reserved.
//

import UIKit

class SearchMapRestaurantTypeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var typeImageView: UIImageView!
    @IBOutlet weak var typeLabel: UILabel!
    
    func configure(with image: UIImage, typeName: String) {
        typeImageView.image = image
        typeLabel.text = typeName
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.cornerRadius()
    }
    
}
