//
//  SearchMapViewController.swift
//  Lobster
//
//  Created by JustDoIt on 22.12.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import UIKit
import GoogleMaps

class SearchMapViewController: UIViewController {
    
    let kSearchMapRestaurantType = "searchMapRestaurantType"
    
    let kNavigationViewBarHideHeight: CGFloat = CGFloat(exactly: 55.0)!
    let kNavigationViewBarShowHeight: CGFloat = CGFloat(exactly: 227.0)!

    @IBOutlet weak var mapView: GMSMapView!

    @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var headerView: UIView! {
        didSet {
            headerView.cornerRadius()
            headerView.alpha = 0.8
        }
    }
    @IBOutlet weak var searchCollectionView: UICollectionView! {
        didSet {
            searchCollectionView.dataSource = self
            searchCollectionView.delegate = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let navigationBarView = NavigationBarView.instanceFromNib()
        navigationBarView.delegate = self
        navigationBarView.translatesAutoresizingMaskIntoConstraints = false
        
        self.headerView.addSubview(navigationBarView)
        
        self.headerView.addConstraint(NSLayoutConstraint.init(item: navigationBarView,
                                                              attribute: .height,
                                                              relatedBy: .equal,
                                                              toItem: self.headerView,
                                                              attribute: .height,
                                                              multiplier: 1.0,
                                                              constant: 0.0))
        self.headerView.addConstraint(NSLayoutConstraint.init(item: navigationBarView,
                                                              attribute: .width,
                                                              relatedBy: .equal,
                                                              toItem: self.headerView,
                                                              attribute: .width,
                                                              multiplier: 1.0,
                                                              constant: 0.0))
        self.headerView.addConstraint(NSLayoutConstraint.init(item: navigationBarView,
                                                              attribute: .top,
                                                              relatedBy: .equal,
                                                              toItem: self.headerView,
                                                              attribute: .top,
                                                              multiplier: 1.0,
                                                              constant: 0.0))
        self.headerView.addConstraint(NSLayoutConstraint.init(item: navigationBarView,
                                                              attribute: .centerX,
                                                              relatedBy: .equal,
                                                              toItem: self.headerView,
                                                              attribute: .centerX,
                                                              multiplier: 1.0,
                                                              constant: 0.0))
    }

}

extension SearchMapViewController: NavigationBarViewDelegate {
    func mapButtonPressed() {
        Coordinator.shared.presentViewController(identifier: .mapViewController, completion: nil)
    }
    
    func searchButtonPressed() {
        Coordinator.shared.presentViewController(identifier: .searchViewController, completion: nil)
    }
    
    func menuButtonPressed() {
        Coordinator.shared.presentViewController(identifier: .menuViewController, completion: nil)
    }
    
    func searchCollectionViewDidShow() {
        self.view.bringSubviewToFront(headerView)
        headerViewHeightConstraint.constant = kNavigationViewBarShowHeight
        UIView.animate(withDuration: kAnimateTime) {
            self.view.layoutIfNeeded()
        }
    }
    
    func searchCollectionViewDidHide() {
        headerViewHeightConstraint.constant = kNavigationViewBarHideHeight
        UIView.animate(withDuration: kAnimateTime) {
            self.view.layoutIfNeeded()
        }
    }
    
    func willHideNavigationBarView() {
        self.view.endEditing(true)
    }
}

extension SearchMapViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let typeCell = collectionView.dequeueReusableCell(withReuseIdentifier: kSearchMapRestaurantType,
                                                             for: indexPath) as? SearchMapRestaurantTypeCollectionViewCell {
            typeCell.configure(with: #imageLiteral(resourceName: "menu-bag"), typeName: "Nigh Club")
            return typeCell
        }
        return UICollectionViewCell()
    }
}
