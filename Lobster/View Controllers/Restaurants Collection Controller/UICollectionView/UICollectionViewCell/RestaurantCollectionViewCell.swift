//
//  RestaurantCollectionViewCell.swift
//  Lobster
//
//  Created by JustDoIt on 25.08.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import UIKit
import SDWebImage
import CoreLocation

class RestaurantCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var backgrndView: UIView!
    @IBOutlet weak var generalImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var averageCheckLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var workTimeLabel: UILabel!

    // Rating Stars
    @IBOutlet weak var ratingStarFirst: UIImageView!
    @IBOutlet weak var ratingStarSecond: UIImageView!
    @IBOutlet weak var ratingStarThird: UIImageView!
    @IBOutlet weak var ratingStarFourth: UIImageView!
    @IBOutlet weak var ratingStarFifth: UIImageView!

    var ratingStars: [UIImageView]!

    override func awakeFromNib() {
        ratingStars = [ratingStarFirst, ratingStarSecond, ratingStarThird, ratingStarFourth, ratingStarFifth]

//        backgrndView.layer.shadowColor = UIColor.black.cgColor
//        backgrndView.layer.shadowOpacity = 0.25
//        backgrndView.layer.shadowOffset = CGSize(width: 0, height: 4)
//        backgrndView.layer.shadowRadius = 10
//        backgrndView.layer.shadowPath = UIBezierPath(rect: backgrndView.bounds).cgPath
//        backgrndView.layer.shouldRasterize = true
        backgrndView.layer.cornerRadius = 8

        generalImageView.layer.cornerRadius = 8

        setRating(number: 64)
    }

    func configure(restaurant: Restaurant) {
        self.titleLabel.text = restaurant.name
        if let url = restaurant.imageLinks.first {
            setImageViewWith(url: url)
        }
        setDistanceBy(lng: restaurant.lng, lat: restaurant.lat)
//        self.averageCheckLabel.text = String(restaurant.averageCheck)
//        self.reviewCountLabel.text = String(restaurant.reviewsCount)
//        self.ratingLabel.text = String(restaurant.rating)
    }

    // MARK: - Private

    private func setImageViewWith(url: String) {
        let imageURL = URL(string: url)
        generalImageView.sd_setImage(with: imageURL, completed: nil)
    }

    private func setDistanceBy(lng: CLLocationDistance, lat: CLLocationDistance) {
        let distance = LocationManager.shared.distanceToLocationBy(lat: lat, lng: lng)
        self.distanceLabel.text = "\(String(describing: distance))км"
    }

    // MARK: Average Check
    // From 0 - 3
    private func setAverageCheck(number: Int) {
        if (number < 0 || number > 3) {
            return
        }
        var moneyString = ""
        for _ in 0..<number {
            moneyString.append("$")
        }
    }

    // MARK: Stars Rating
    // From 0 - 5
    private func setRating(number: Int) {
        if (number < 0 || number > 5) {
            return
        }
        for index in 0..<5 {
            if (index < number) {
                starIsEnabled(starNumber: index, isEnabled: true)
            } else {
                starIsEnabled(starNumber: index, isEnabled: false)
            }
        }
    }

    private func starIsEnabled(starNumber: Int, isEnabled: Bool) {
        if (starNumber < 0 || starNumber > 5) {
            return
        }
        if isEnabled == true {
            ratingStars[starNumber].image = UIImage(named: "star-on")
        } else {
            ratingStars[starNumber].image = UIImage(named: "star-off")
        }
    }

}
