//
//  TypeCollectionViewCell.swift
//  Lobster
//
//  Created by JustDoIt on 02.09.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import UIKit

class TypeCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backgrndView: UIView!

    func configure(title: String) {
        titleLabel.text = title
    }

    func setState(isVisible: Bool) {
        if isVisible == true {
            backgrndView.layer.borderColor = UIColor.redDefault.cgColor
            backgrndView.layer.borderWidth = 2.0
            titleLabel.textColor = UIColor.redDefault
        } else {
            backgrndView.layer.borderColor = UIColor.redDefault.cgColor
            backgrndView.layer.borderWidth = 0.0
            titleLabel.textColor = .black
        }
    }

}
