//
//  RestaurantsCollectionManageActions.swift
//  Lobster
//
//  Created by JustDoIt on 10.09.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import Foundation

extension RestaurantsCollectionViewController {

    @IBAction func userButtonPressed() {
        Coordinator.shared.setRootViewControllerWith(identifier: .signInViewController, modalTransitionStyle: nil)
        UserDefaults.standard.removeObject(forKey: kAccessToken)
    }

    func getRestaurants() {
        DataManager.shared.getRestaurant(params: ["": ""]) { (response, error) in
            if (error != nil) {
                return
            }
            self.restaurantsList = response as? [Restaurant]
            self.restaurantsCollectionView.reloadData()
        }
    }
    
    func openRestaurant(_ restaurant: Restaurant!) {
        if let restaurantViewController = Coordinator.instantiateViewController(with: .restaurantViewController) as? RestaurantViewController {
            restaurantViewController.restaurant = restaurant
            Coordinator.shared.presentViewController(viewController: restaurantViewController, modalTransitionStyle: nil, completion: nil)
        }
    }
    
}
