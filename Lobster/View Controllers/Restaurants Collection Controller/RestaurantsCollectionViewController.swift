//
//  RestaurantsCollectionViewController.swift
//  Lobster
//
//  Created by JustDoIt on 25.08.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import UIKit

class RestaurantsCollectionViewController: UIViewController {

    @IBOutlet weak var navigationBarView: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var typeCollectionView: UICollectionView!
    @IBOutlet weak var restaurantsCollectionView: UICollectionView!

    let kTypeCell = "TypeCell"
    let kRestaurantCell = "RestaurantCell"

    var restaurantsList: [Restaurant]!

    var currentRestarauntType: String!
    var restaurantTypes: [String] = [String]()
    var currentRestarauntTypeIndexPath: IndexPath!

    override func loadView() {
        super.loadView()
        restaurantTypes = ["Azerbaijan", "Ukraine", "Russia", "Kazahstan", "China", "Japanesse"]
        searchBar.delegate = self
        getRestaurants()
    }

    @IBAction func searchButtonPressed(_ sender: Any) {
        self.navigationBarView.isHidden = true
        self.searchBar.isHidden = false
        UIView.animate(withDuration: kAnimateTime) {
            self.navigationBarView.alpha = 0.0
            self.searchBar.alpha = 1.0
        }
    }

}

extension RestaurantsCollectionViewController: UICollectionViewDataSource, UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.restaurantsCollectionView {
            if restaurantsList != nil {
                return restaurantsList.count
            } else {
                return 0
            }
        } else {
            return restaurantTypes.count
        }
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.restaurantsCollectionView {
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kRestaurantCell,
                                                             for: indexPath) as? RestaurantCollectionViewCell {
                if restaurantsList != nil {
                    cell.configure(restaurant: restaurantsList[indexPath.row])
                }
                return cell
            }
        } else {
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kTypeCell,
                                                             for: indexPath) as? TypeCollectionViewCell {
                cell.configure(title: restaurantTypes[indexPath.row])
                return cell
            }
        }
        return UICollectionViewCell()
    }

    func collectionView(_ collectionView: UICollectionView,
                        willDisplay cell: UICollectionViewCell,
                        forItemAt indexPath: IndexPath) {
        if collectionView == self.restaurantsCollectionView {

        } else {
            if indexPath == currentRestarauntTypeIndexPath {
                if let cell = cell as? TypeCollectionViewCell {
                    cell.setState(isVisible: true)
                }
            } else {
                if let cell = cell as? TypeCollectionViewCell {
                    cell.setState(isVisible: false)
                }
            }
        }
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.restaurantsCollectionView {
            let restaurant = restaurantsList[indexPath.row]
            openRestaurant(restaurant)
        } else {
            setRestarauntType(indexPath: indexPath)
        }
    }

}

extension RestaurantsCollectionViewController: UISearchBarDelegate {

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.navigationBarView.isHidden = false
        self.searchBar.isHidden = true
        UIView.animate(withDuration: kAnimateTime) {
            self.navigationBarView.alpha = 1.0
            self.searchBar.alpha = 0.0
        }
    }

}
