//
//  RestaurantsCollectionManageStates.swift
//  Lobster
//
//  Created by JustDoIt on 02.09.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import Foundation

extension RestaurantsCollectionViewController {

    func setRestarauntType(indexPath: IndexPath) {
        currentRestarauntType = restaurantTypes[indexPath.row]
        currentRestarauntTypeIndexPath = indexPath
        // Set off all visible cells
        // If last switched on cell isn't visible, it will be switched off in UICollectionView:willDisplay()
        for cell in typeCollectionView.visibleCells {
            if let cell = cell as? TypeCollectionViewCell {
                cell.setState(isVisible: false)
            }
        }
        // Set New Cell
        if let currentCell = typeCollectionView.cellForItem(at: indexPath) as? TypeCollectionViewCell {
            currentCell.setState(isVisible: true)
        }
    }

    func setSearchBarVisible(isVisible: Bool) {
        if isVisible == true {
            searchBar.isHidden = false
        } else {

        }
    }

}
