//
//  InitialViewController.swift
//  Lobster
//
//  Created by Rauf on 24.09.17.
//  Copyright © 2017 RGS. All rights reserved.
//

import UIKit
import AVFoundation
import CoreNFC
import SwiftyJSON

enum Animation: String {
    case qrCode = "/QRCodeInstruction.mp4"
    case nfc = "/NFCInstruction.mp4"
}

enum ScannerViewState: Int {
    case qrCode, nfc
}

class ScannerViewController: UIViewController {

    var currentState: ScannerViewState!

    var videoPlayer: AVQueuePlayer!
    var playerLooper: AVPlayerLooper!
    var videoLayer: AVCaptureVideoPreviewLayer!

    var nfcModule: NFCModule!
    var qrCodeModule: QRCodeModule!

    var scanQRCodeLabel: UILabel!
    var changeStateButton: UIButton!

    // MARK: - Life Cycle
    override func loadView() {
        super.loadView()
        self.title = "Scanner"
        self.view.backgroundColor = UIColor(red: 0.94, green: 0.94, blue: 0.96, alpha: 1.0)
        self.tabBarItem = UITabBarItem.init(title: "", image: UIImage.init(named: "QRCode_button"), tag: 0)

        // Create UILabel
        scanQRCodeLabel = UILabel()
        scanQRCodeLabel.text = "Scan the QR code on the table."
        scanQRCodeLabel.textColor = UIColor.darkGray
        scanQRCodeLabel.font = UIFont.systemFont(ofSize: 16)
        scanQRCodeLabel.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(scanQRCodeLabel)

        // Create UIButton
        changeStateButton = UIButton()
        changeStateButton.setTitle("Scan QR Code", for: .normal)
        changeStateButton.setTitleColor(.darkGray, for: .normal)
        changeStateButton.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        changeStateButton.contentHorizontalAlignment = .left
        changeStateButton.contentEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
        changeStateButton.layer.borderWidth = 0.4
        changeStateButton.layer.borderColor = UIColor.lightGray.cgColor
        changeStateButton.backgroundColor = .white
        changeStateButton.translatesAutoresizingMaskIntoConstraints = false
        let buttonTouchUpInside = UITapGestureRecognizer(target: self, action: #selector(changeStateButtonPressed))
        changeStateButton.addGestureRecognizer(buttonTouchUpInside)
        self.view.addSubview(changeStateButton)

        // Create UIImageView
        let changeStateButtonImage = UIImageView(image: #imageLiteral(resourceName: "arrow"))
        changeStateButtonImage.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(changeStateButtonImage)

        // Set Constraints
        let labelXConstraint = NSLayoutConstraint.init(item: scanQRCodeLabel, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1.0, constant: 0)
        let labelYConstraint = NSLayoutConstraint.init(item: scanQRCodeLabel, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1.0, constant: 45)

        let changeStateButtonXConstraint = NSLayoutConstraint.init(item: changeStateButton, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1.0, constant: 0)
        let changeStateButtonYConstraint = NSLayoutConstraint.init(item: changeStateButton, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1.0, constant: -80)
        let changeStateButtonHeightConstraint = NSLayoutConstraint.init(item: changeStateButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 60)
        let changeStateButtonWidthConstraint = NSLayoutConstraint.init(item: changeStateButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: self.view.frame.width)

        let changeStateButtonImageCenterYConstraint = NSLayoutConstraint.init(item: changeStateButtonImage, attribute: .centerY, relatedBy: .equal, toItem: changeStateButton, attribute: .centerY, multiplier: 1.0, constant: 0)
        let changeStateButtonImageRightConstraint = NSLayoutConstraint.init(item: changeStateButtonImage, attribute: .right, relatedBy: .equal, toItem: changeStateButton, attribute: .right, multiplier: 1.0, constant: -20)
        let changeStateButtonImageHeightConstraint = NSLayoutConstraint.init(item: changeStateButtonImage, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 15)
        let changeStateButtonImageWidthConstraint = NSLayoutConstraint.init(item: changeStateButtonImage, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 8.5)

        self.view.addConstraint(labelXConstraint)
        self.view.addConstraint(labelYConstraint)

        self.view.addConstraint(changeStateButtonXConstraint)
        self.view.addConstraint(changeStateButtonYConstraint)
        self.view.addConstraint(changeStateButtonHeightConstraint)
        self.view.addConstraint(changeStateButtonWidthConstraint)

        self.view.addConstraint(changeStateButtonImageCenterYConstraint)
        self.view.addConstraint(changeStateButtonImageRightConstraint)
        self.view.addConstraint(changeStateButtonImageHeightConstraint)
        self.view.addConstraint(changeStateButtonImageWidthConstraint)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        nfcModule = NFCModule()
        nfcModule.delegate = self

        qrCodeModule = QRCodeModule()
        qrCodeModule.delegate = self

        cameraViewForQRCode()
        changeState(state: ScannerViewState.qrCode)
    }

    // QR Code Camera View
    func cameraViewForQRCode() {
        guard (qrCodeModule.session != nil) else {
            return
        }
        videoLayer = AVCaptureVideoPreviewLayer(session: qrCodeModule.session)
        videoLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        videoLayer.frame = CGRect.init(x: 0, y: view.frame.height/4, width: view.frame.width, height: view.frame.height/2)
        videoLayer.isHidden = true
        self.view.layer.addSublayer(videoLayer)
    }

    @objc func changeStateButtonPressed() {
        switch currentState {
        case .nfc?:
            self.changeState(state: .qrCode)
        case .qrCode?:
            self.changeState(state: .nfc)
        default:
            return
        }
    }

}

extension ScannerViewController: ScannerDelegate {
    func scanner(getResposne response: JSON) {
        print(response)
    }
}
