//
//  QRCodeModule.swift
//  Lobster
//
//  Created by JustDoIt on 04.08.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import Foundation
import SwiftyJSON
import AVFoundation

class QRCodeModule: NSObject, AVCaptureMetadataOutputObjectsDelegate {

    var session: AVCaptureSession!
    var delegate: ScannerDelegate?

    override init() {
        super.init()
        self.session = AVCaptureSession()
        guard let device = AVCaptureDevice.default(for: .video) else {
            return
        }
        do {
            let input = try AVCaptureDeviceInput.init(device: device)
            session.addInput(input)
        } catch {
            return
        }
        let output = AVCaptureMetadataOutput()
        output.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        session.addOutput(output)
        output.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
        session.startRunning()
    }

    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        if (!metadataObjects.isEmpty) {
            if let object = metadataObjects[0] as? AVMetadataMachineReadableCodeObject,
                object.type == AVMetadataObject.ObjectType.qr, let response = object.stringValue {
                delegate?.scanner(getResposne: JSON(response))
            }
        }
    }

}
