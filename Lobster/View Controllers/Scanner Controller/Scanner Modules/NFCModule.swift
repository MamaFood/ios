//
//  NFCModule.swift
//  Lobster
//
//  Created by JustDoIt on 04.08.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import Foundation
import SwiftyJSON
import CoreNFC

class NFCModule: NSObject, NFCNDEFReaderSessionDelegate {

    var nfcSession: NFCNDEFReaderSession!
    var delegate: ScannerDelegate?

    override init() {
        super.init()
        self.nfcSession = NFCNDEFReaderSession(delegate: self, queue: DispatchQueue.global(qos: .background), invalidateAfterFirstRead: true)
    }

    func start() {
        guard (self.nfcSession != nil) else {
            return
        }
        self.nfcSession.begin()
    }

    func stop() {
        guard (self.nfcSession != nil) else {
            return
        }
        self.nfcSession.invalidate()
    }

    func readerSession(_ session: NFCNDEFReaderSession, didInvalidateWithError error: Error) {
        print(error.localizedDescription)
        print(session)
    }

    func readerSession(_ session: NFCNDEFReaderSession, didDetectNDEFs messages: [NFCNDEFMessage]) {
        if (messages.count > 0) {
            let message = messages[0]
            if (message.records.count > 0) {
                let record = message.records[0]
                let payload = JSON(record.payload)
                print(payload)
                delegate?.scanner(getResposne: payload)
            }
        }
    }
}
