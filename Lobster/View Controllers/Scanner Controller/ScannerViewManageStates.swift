//
//  ScannerViewManageStates.swift
//  Lobster
//
//  Created by JustDoIt on 06.08.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import Foundation
import AVFoundation
import UIKit

extension ScannerViewController {

    func changeState(state: ScannerViewState) {
        switch state {
        case .nfc:
            activateNfcState()
        case .qrCode:
            activateQrCodeState()
        }
        currentState = state
    }

    private func activateNfcState() {
        animationFor(animation: Animation.nfc)
        changeStateButton.setTitle("Scan QR Code", for: .normal)
        scanQRCodeLabel.text = "Scan the NFC pin on the table."
        self.nfcModule.start()
        videoLayer.isHidden = true
    }

    private func activateQrCodeState() {
        animationFor(animation: Animation.qrCode)
        changeStateButton.setTitle("Scan NFC", for: .normal)
        scanQRCodeLabel.text = "Scan the QR code on the table."
        self.nfcModule.stop()
        videoLayer.isHidden = false
    }

    private func animationFor(animation: Animation) {
        if let filepath = Bundle.main.path(forResource: animation.rawValue, ofType: nil) {
            let fileURL = URL(fileURLWithPath: filepath)
            if (videoPlayer != nil) {
                videoPlayer.removeAllItems()
                let videoPlayerItem = AVPlayerItem(url: fileURL)
                videoPlayer.insert(videoPlayerItem, after: nil)
                playerLooper = AVPlayerLooper(player: videoPlayer, templateItem: videoPlayerItem)
            } else {
                let videoPlayerItem = AVPlayerItem(url: fileURL)
                videoPlayer = AVQueuePlayer(items: [videoPlayerItem])
                playerLooper = AVPlayerLooper(player: videoPlayer, templateItem: videoPlayerItem)
            }
            _ = try? AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: .mixWithOthers)

            let playerLayer = AVPlayerLayer(player: videoPlayer)
            playerLayer.frame = CGRect.init(x: (view.frame.width - view.frame.width/1.5)/2,
                                            y: (view.frame.height/2-view.frame.height/3)/2+view.frame.height/4,
                                            width: view.frame.width/1.5,
                                            height: view.frame.height/3)
            playerLayer.backgroundColor = UIColor.darkGray.cgColor
            playerLayer.videoGravity = .resizeAspectFill
            playerLayer.cornerRadius = 10.0
            playerLayer.masksToBounds = true
            self.view.layer.addSublayer(playerLayer)
            videoPlayer.play()
        }
    }

}
