//
//  CheckListViewCollectionViewCell.swift
//  Lobster
//
//  Created by JustDoIt on 10.10.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import UIKit

class CheckListViewCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var generalImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!

    func configure(with food: Dish!) {
        if let imageURL = food.imageLinks.first {
            setImageViewWith(url: imageURL)
        }
        self.titleLabel.text = food.title
        self.descriptionLabel.text = food.description
        self.priceLabel.text = LocalizeHelper.shared.convertDoubleToCurrency(double: food.price)
    }

    // MARK: - Private

    private func setImageViewWith(url: String) {
        let imageURL = URL(string: url)
        generalImageView.sd_setImage(with: imageURL, completed: nil)
    }

}
