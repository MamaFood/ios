//
//  CheckListViewController.swift
//  Lobster
//
//  Created by JustDoIt on 10.10.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import UIKit

class CheckListViewController: UIViewController {

    @IBOutlet weak var orderNumberLabel: UILabel!
    @IBOutlet weak var orderDateLabel: UILabel!
    @IBOutlet weak var dishesCollectionView: UICollectionView!
    @IBOutlet weak var totalPriceLabel: UILabel!
    @IBOutlet weak var payButtonPressed: UIButton!

    private var dishes: [Dish]! = [Dish]()

    override func viewDidLoad() {
        super.viewDidLoad()
        orderNumberLabel.text = "Choose some dishes 🌯🌮🍔🍕"
        totalPriceLabel.text = "\(totalPriceOfDishes())"
        payButtonPressed.isEnabled = false
    }

    func addDish(dish: Dish) {
        dishes.append(dish)

        orderNumberLabel.text = "My Order - #4217"
        payButtonPressed.isEnabled = true

        guard totalPriceLabel != nil else { return }
        totalPriceLabel.text = "\(totalPriceOfDishes())"
        dishesCollectionView.reloadData()
    }

    func removeDish(index: Int) {
        dishes.remove(at: index)
        guard totalPriceLabel != nil else { return }
        totalPriceLabel.text = "\(totalPriceOfDishes())"
        dishesCollectionView.reloadData()
    }

    private func totalPriceOfDishes() -> Double {
        var totalPrice = 0.0
        for dish in dishes {
           totalPrice += dish.price
        }
        return totalPrice
    }

    @IBAction func checkoutButtonPressed(_ sender: Any) {

    }

}

extension CheckListViewController: UICollectionViewDataSource, UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dishes.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "dishCell", for: indexPath) as? CheckListViewCollectionViewCell {
        cell.configure(with: dishes[indexPath.row])
        return cell
        }
        return UICollectionViewCell()
    }

}
