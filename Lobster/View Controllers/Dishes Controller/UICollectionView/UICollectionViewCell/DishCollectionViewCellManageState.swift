//
//  DishViewManageState.swift
//  Lobster
//
//  Created by JustDoIt on 09.12.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import UIKit

extension DishCollectionViewCell {

    func setLikeState(state: Bool) {
        if state {
            let image = UIImage(named: likeImageIdle)
            self.likeButton.setImage(image, for: .normal)
        } else {
            let image = UIImage(named: likeImageSelected)
            self.likeButton.setImage(image, for: .normal)
        }
    }

}
