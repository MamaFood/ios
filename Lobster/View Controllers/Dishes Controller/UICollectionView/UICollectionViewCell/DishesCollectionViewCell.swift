//
//  DishesCollectionViewCell.swift
//  Lobster
//
//  Created by JustDoIt on 10.10.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import UIKit

protocol DishesCollectionViewCellDelegate {
    func addToCheckButtonPressed(cell: DishesCollectionViewCell)
}

class DishesCollectionViewCell: UICollectionViewCell {

    var delegate: DishesCollectionViewCellDelegate!

    @IBOutlet weak var generalImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!

    func configure(with dish: Dish!) {
        if let imageURL = dish.imageLinks.first {
            setImageViewWith(url: imageURL)
        }
        self.titleLabel.text = dish.title
        self.descriptionLabel.text = dish.description
        self.priceLabel.text = LocalizeHelper.shared.convertDoubleToCurrency(double: dish.price)
    }

    @IBAction func addToCheck(_ sender: Any) {
        guard delegate != nil else {
            return
        }
        delegate.addToCheckButtonPressed(cell: self)
    }

    // MARK: - Private

    private func setImageViewWith(url: String) {
        let imageURL = URL(string: url)
        generalImageView.sd_setImage(with: imageURL, completed: nil)
    }

}
