//
//  DishView.swift
//  Lobster
//
//  Created by JustDoIt on 08.12.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import UIKit
import Alamofire

class DishCollectionViewCell: UICollectionViewCell {

    let likeImageIdle = "like-idle"
    let likeImageSelected = "like-selected"

    @IBOutlet weak var dishImageView: UIImageView!
    @IBOutlet weak var likeButton: UIButton! {
        didSet {
            likeButton.layer.masksToBounds = false
            likeButton.layer.shadowColor = UIColor.white.cgColor
            likeButton.layer.shadowOffset = CGSize(width: 5, height: 5)
            likeButton.layer.shadowRadius = 5.0
            likeButton.layer.shadowOpacity = 1.0
        }
    }
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var supportLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel! {
        didSet {
            ratingLabel.textColor = UIColor.lightGray
        }
    }
    @IBOutlet weak var ratingLabelBackgroundView: UIView! {
        didSet {
            // Corner and Border Width
            ratingLabelBackgroundView.layer.borderColor = UIColor.lightGray.cgColor
            ratingLabelBackgroundView.layer.borderWidth = CGFloat(integerLiteral: 1)
            ratingLabelBackgroundView.layer.cornerRadius = 5.0
            ratingLabelBackgroundView.clipsToBounds = true
        }
    }

    var likeButtonState: Bool = false

    func configure(with dish: Dish) {
        self.titleLabel.text = dish.title
        self.descriptionLabel.text = dish.description
        self.supportLabel.text = "7.9km"

        guard let imageLink = dish.imageLinks.first else {
            return
        }
        Alamofire.request(imageLink).responseData { (responseData) in
            self.dishImageView.image = UIImage(data: responseData.data ?? Data())
        }
    }

    @IBAction func likeButtonPressed(_ sender: Any) {
        if likeButtonState {
            likeButtonState = false
            setLikeState(state: likeButtonState)
        } else {
            likeButtonState = true
            setLikeState(state: likeButtonState)
        }
    }
}
