//
//  MenuControllerManageStates.swift
//  Lobster
//
//  Created by JustDoIt on 06.10.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import Foundation

extension DishesViewController {

    func setRestarauntType(indexPath: IndexPath) {
        currentType = types[indexPath.row]
        currentTypeIndexPath = indexPath
        // Set off all visible cells
        // If last switched on cell isn't visible, it will be switched off in UICollectionView:willDisplay()
        for cell in typesCollectionView.visibleCells {
            if let cell = cell as? TypeCollectionViewCell {
                cell.setState(isVisible: false)
            }
        }
        // Set New Cell
        if let currentCell = typesCollectionView.cellForItem(at: indexPath) as? TypeCollectionViewCell {
            currentCell.setState(isVisible: true)
        }
    }

}
