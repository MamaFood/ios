//
//  DishesViewController.swift
//  Lobster
//
//  Created by JustDoIt on 01.10.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import UIKit

class DishesViewController: UIViewController {

    let kDishCell = "dishCell"

    var restaurant: Restaurant!

    var dishes: [Dish]!

    @IBOutlet weak var typesCollectionView: UICollectionView!
    @IBOutlet weak var dishesCollectionView: UICollectionView! {
        didSet {
            dishesCollectionView.register(UINib(nibName: "DishCollectionViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: kDishCell)
        }
    }

    var types: [String]!
    var currentType: String!
    var currentTypeIndexPath: IndexPath!

    // CollectionView Setting
    let kCellHeight: CGFloat = 128
    let kInset: CGFloat = 10

    override func loadView() {
        super.loadView()
        self.getDishes(restaurantId: restaurant.id)
        types = ["Azerbaijan", "Ukraine", "Russia", "Kazahstan", "China", "Japanesse"]
    }

}

extension DishesViewController: UICollectionViewDelegate, UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == typesCollectionView {
            if (types != nil) {
                return types.count
            } else {
                return 0
            }
        } else {
            if (dishes != nil) {
                return dishes.count
            } else {
                return 0
            }
        }
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if collectionView == self.dishesCollectionView {
            return 2
        } else {

        }
        return 1
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == typesCollectionView {
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "typeCell",
                                                             for: indexPath) as? TypeCollectionViewCell {
            cell.configure(title: types[indexPath.row])
            return cell
            }
        } else {
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kDishCell,
                                                             for: indexPath) as? DishCollectionViewCell {
            cell.configure(with: dishes[indexPath.row])
//            cell.delegate = self
            return cell
            }
        }
        return UICollectionViewCell()
    }

    func collectionView(_ collectionView: UICollectionView,
                        willDisplay cell: UICollectionViewCell,
                        forItemAt indexPath: IndexPath) {
        if collectionView == self.dishesCollectionView {

        } else {
            if indexPath == currentTypeIndexPath {
                if let cell = cell as? TypeCollectionViewCell {
                    cell.setState(isVisible: true)
                }
            } else {
                if let cell = cell as? TypeCollectionViewCell {
                    cell.setState(isVisible: false)
                }
            }
        }
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.dishesCollectionView {

        } else {
            setRestarauntType(indexPath: indexPath)
        }
    }

}

extension DishesViewController: DishesCollectionViewCellDelegate {

    func addToCheckButtonPressed(cell: DishesCollectionViewCell) {
        guard let index = dishesCollectionView.indexPath(for: cell)?.row else { return }
        addDishToCart(dishes[index])
    }

}

extension DishesViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == self.dishesCollectionView {
            return CGSize(width: UIScreen.main.bounds.width/2-25, height: 180)
        } else {
            if let collectionViewLayout = collectionViewLayout as? UICollectionViewFlowLayout {
                if let collectionViewLayout = collectionViewLayout as? UICollectionViewFlowLayout {
                    return collectionViewLayout.itemSize
                }
            }
        }
        return CGSize.zero
    }

}
