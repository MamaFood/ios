//
//  MenuControllerManageActions.swift
//  Lobster
//
//  Created by JustDoIt on 01.10.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import Foundation

extension DishesViewController {

    func getDishes(restaurantId: Int) {
        DataManager.shared.getDishes(params: ["restaurantId": restaurantId]) { (response, error) in
            if (error != nil) {
                return
            }
            self.dishes = response as? [Dish]
            self.dishesCollectionView.reloadData()
        }
    }

    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}
