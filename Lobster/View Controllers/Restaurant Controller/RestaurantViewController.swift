//
//  RestaurantController.swift
//  Lobster
//
//  Created by JustDoIt on 09.09.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import UIKit

struct MenuButton {
    var color: UIColor
    var title: String
    var image: UIImage
    
    init(color: UIColor, title: String, image: UIImage!) {
        self.color = color
        self.title = title
        if let image = image { self.image = image } else { self.image = UIImage() }
    }
}

class RestaurantViewController: UIViewController {
    
    let kImageCellIdentifier = "imageCellIdentifier"
    let kInfoCellIdentifier = "infoCellIdentifier"
    let kMenuCellIdentifier = "menuCellIdentifier"
    let kDishCellIdentifier = "dishCellIdentifier"
    let kReviewCellIdentifier = "reviewCellIdentifier"

    var restaurant: Restaurant!
    var popularMenu: [Dish]!
    var reviews: [Review]!
    var liked: Bool = false
    var expertReviewsSelected: Bool = false
    var info: [String:String]!
    var restaurantImages: [UIImage] = [#imageLiteral(resourceName: "startup-background-3"),#imageLiteral(resourceName: "startup-background-3"),#imageLiteral(resourceName: "startup-background-3"),#imageLiteral(resourceName: "startup-background-3")]
    var menuButtons: [MenuButton] = [MenuButton.init(color: .redDefault, title: "Menu", image: #imageLiteral(resourceName: "menu")),
                                     MenuButton.init(color: .blueDefault, title: "Reservation", image: #imageLiteral(resourceName: "menu")),
                                     MenuButton.init(color: .carrotDefault, title: "Burgers", image: #imageLiteral(resourceName: "menu")),
                                     MenuButton.init(color: .greenDefault, title: "Desert", image: #imageLiteral(resourceName: "menu"))]

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageViewsCollectionView: UICollectionView! {
        didSet {
            imageViewsCollectionView.shadow(opacity: 0.5)
            imageViewsCollectionView.delegate = self
            imageViewsCollectionView.dataSource = self
        }
    }
    @IBOutlet weak var imageViewsPageControl: UIPageControl! {
        didSet {
            imageViewsPageControl.numberOfPages = restaurantImages.count
            imageViewsPageControl.currentPage = 0
        }
    }
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var reviewAddButton: UIButton! {
        didSet {
            reviewAddButton.border(color: .redDefault, width: 1.0)
            reviewAddButton.cornerRadius()
        }
    }
    @IBOutlet weak var bestSellerImageView: UIImageView! {
        didSet {
            bestSellerImageView.cornerRadius()
//            bestSellerImageView.shadow()
        }
    }
    @IBOutlet weak var expertReviewButton: UIButton! {
        didSet {
            expertReviewButton.border(color: .redDefault, width: 1.0)
            expertReviewButton.cornerRadius()
        }
    }
    @IBOutlet weak var userReviewButton: UIButton! {
        didSet {
            userReviewButton.border(color: .redDefault, width: 1.0)
            userReviewButton.cornerRadius()
        }
    }
    
    @IBOutlet weak var infoCollectionView: UICollectionView! {
        didSet {
            infoCollectionView.dataSource = self
            infoCollectionView.delegate = self
            if let flowLayout = infoCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
                flowLayout.estimatedItemSize = CGSize(width: infoCollectionView.frame.width/3, height: 63)
            }
        }
    }
    @IBOutlet weak var menuCollectionView: UICollectionView! {
        didSet {
            menuCollectionView.dataSource = self
            menuCollectionView.delegate = self
        }
    }
    @IBOutlet weak var dishesCollectionView: UICollectionView! {
        didSet {
            dishesCollectionView.dataSource = self
            dishesCollectionView.delegate = self
        }
    }
    @IBOutlet weak var reviewCollectionView: UICollectionView! {
        didSet {
            reviewCollectionView.dataSource = self
            reviewCollectionView.delegate = self
        }
    }
    @IBOutlet weak var progressView: UIProgressView! {
        didSet {
            progressView.transform = CGAffineTransform(scaleX: 1, y: 11)
        }
    }
    
    var selectedReviewState: SelectedReviewState = .expert

    override func loadView() {
        super.loadView()
        configure(with: restaurant)
    }
}

extension RestaurantViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.imageViewsPageControl.currentPage = indexPath.row
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.imageViewsCollectionView {
            return restaurantImages.count
        }
        else if collectionView == self.infoCollectionView {
            return 3
        } else if collectionView == self.menuCollectionView {
            return menuButtons.count
        } else if collectionView == self.dishesCollectionView {
            return 3
        } else if collectionView == self.reviewCollectionView {
            return 2
        }
        return 0
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == imageViewsCollectionView {
          if let imageViewCollectionViewCell = collectionView.dequeueReusableCell(
            withReuseIdentifier: kImageCellIdentifier,
            for: indexPath) as? ImageCollectionViewCell {
            imageViewCollectionViewCell.configure(image: restaurantImages[indexPath.row])
            return imageViewCollectionViewCell
            }
        } else if collectionView == self.infoCollectionView {
            if let infoCollectionViewCell = collectionView.dequeueReusableCell(
                withReuseIdentifier: kInfoCellIdentifier,
                for: indexPath) as? InfoCollectionViewCell {
                infoCollectionViewCell.configure(title: Array(info)[indexPath.row].key,
                                                 contain: Array(info)[indexPath.row].value)
                return infoCollectionViewCell
            }
        } else if collectionView == self.menuCollectionView {
            if let menuCollectionViewCell = collectionView.dequeueReusableCell(
                withReuseIdentifier: kMenuCellIdentifier,
                for: indexPath) as? MenuSelectCollectionViewCell {
                menuCollectionViewCell.configure(button: menuButtons[indexPath.row])
                return menuCollectionViewCell
            }
        } else if collectionView == self.dishesCollectionView {
            if let dishesCollectionViewCell = collectionView.dequeueReusableCell(
                withReuseIdentifier: kDishCellIdentifier,
                for: indexPath) as? FoodCollectionViewCell {
                dishesCollectionViewCell.configure(with: TempMenu.sharedInstance.getDish())
                return dishesCollectionViewCell
            }
        } else if collectionView == self.reviewCollectionView {
            if let reviewCollectionViewCell = collectionView.dequeueReusableCell(
                withReuseIdentifier: kReviewCellIdentifier,
                for: indexPath) as? ReviewCollectionViewCell {
//                reviewCollectionViewCell.configure(with: reviews[indexPath.row])
                return reviewCollectionViewCell
            }
        }
        return UICollectionViewCell()
    }

}
