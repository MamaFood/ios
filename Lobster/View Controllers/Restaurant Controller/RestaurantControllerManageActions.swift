//
//  RestaurantControllerManageActions.swift
//  Lobster
//
//  Created by JustDoIt on 18.09.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import UIKit

extension RestaurantViewController {
    
    enum SelectedReviewState {
        case expert, user
    }

    func configure(with restaurant: Restaurant) {
        // generalImageView
        if let url = restaurant.imageLinks.first {
            setImageViewWith(url: url)
        }
        // titleLabel
        self.titleLabel.text = restaurant.name
        // addressLabel
        self.addressLabel.text = "ул. Пушкинская 56А"
        switch self.selectedReviewState {
        case .expert:
            self.expertReviewButton.enableButton()
            self.userReviewButton.disableButton()
        case .user:
            self.expertReviewButton.enableButton()
            self.userReviewButton.disableButton()
        }
        if restaurant.liked {
            self.likeButton.setImage(#imageLiteral(resourceName: "like-selected"), for: .normal)
        } else {
            self.likeButton.setImage(#imageLiteral(resourceName: "like-idle-1"), for: .normal)
        }
        if expertReviewsSelected {
            self.expertReviewButton.enableButton()
            self.userReviewButton.disableButton()
        } else {
            self.userReviewButton.enableButton()
            self.expertReviewButton.disableButton()
        }
        // info
        info = ["Rating" : "\(restaurant.rating ?? 0.0)",
                "Average check" : "\(restaurant.averageCheck ?? 0.0)",
                "Timings" : "\(restaurant.time ?? "")"]
        self.infoCollectionView.reloadData()
        getDishes(restaurantId: restaurant.id)
    }
    
    @IBAction func expertReviewsButtonPressed(_ sender: Any) {
        self.expertReviewsSelected = true
        self.expertReviewButton.enableButton()
        self.userReviewButton.disableButton()
    }
    
    @IBAction func userReviewsButtonPressed(_ sender: Any) {
        self.expertReviewsSelected = false
        self.userReviewButton.enableButton()
        self.expertReviewButton.disableButton()
    }

    private func getDishes(restaurantId: Int) {
        DataManager.shared.getDishes(params: ["restaurantId": restaurantId]) { (response, error) in
            if (error != nil) {
                return
            }
            self.popularMenu = response as? [Dish]
            self.popularMenu = TempMenu.sharedInstance.getDishes(count: 15)
            self.menuCollectionView.reloadData()
        }
    }

    private func setImageViewWith(url: String) {
        let imageURL = URL(string: url)
//        generalImageView.sd_setImage(with: imageURL, completed: nil)
    }
    
    @IBAction func likeButtonPressed(_ sender: Any) {
        self.liked = !self.liked
        if self.liked {
            self.likeButton.setImage(#imageLiteral(resourceName: "like-selected"), for: .normal)
        } else {
            self.likeButton.setImage(#imageLiteral(resourceName: "like-idle-1"), for: .normal)
        }
    }
    
    @IBAction func reviewAddButtonPressed(_ sender: Any) {
    }

    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}
