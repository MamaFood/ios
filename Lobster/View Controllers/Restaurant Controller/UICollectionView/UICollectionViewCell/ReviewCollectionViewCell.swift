//
//  ReviewCollectionViewCell.swift
//  Lobster
//
//  Created by JustDoIt on 25.09.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import UIKit

class ReviewCollectionViewCell: UICollectionViewCell {
    
    let kButtonCell = "buttonCell"

    @IBOutlet weak var avatarImageView: UIImageView! {
        didSet {
            avatarImageView.circle()
        }
    }
    @IBOutlet weak var buttonsCollectionView: UICollectionView! {
        didSet {
            buttonsCollectionView.dataSource = self
            buttonsCollectionView.delegate = self
        }
    }
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.cornerRadius()
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.grayDefault.cgColor
    }

    func configure(with review: Review!) {

    }
}

extension ReviewCollectionViewCell: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let buttonCell = collectionView.dequeueReusableCell(withReuseIdentifier: kButtonCell, for: indexPath) as? ReviewButtonCollectionViewCell {
            buttonCell.configure(text: "50 Likes", image: #imageLiteral(resourceName: "like2-idle"))
            return buttonCell
        }
        return UICollectionViewCell()
    }
    
}
