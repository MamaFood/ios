//
//  MenuSelectCollectionViewCell.swift
//  Lobster
//
//  Created by JustDoIt on 19.01.2019.
//  Copyright © 2019 RGS. All rights reserved.
//

import UIKit

class MenuSelectCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var menuImageView: UIImageView!
    @IBOutlet weak var menuTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.cornerRadius()
        self.shadow()
    }
    
    func configure(button: MenuButton) {
        self.backgroundColor = button.color
        menuImageView.image = button.image
        menuTitleLabel.text = button.title
    }
}
