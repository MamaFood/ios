//
//  ImageCollectionViewCell.swift
//  Lobster
//
//  Created by JustDoIt on 26.01.2019.
//  Copyright © 2019 RGS. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.imageView.image = #imageLiteral(resourceName: "startup-background-3")
    }
    
    func configure(image: UIImage!) {
        self.imageView.image = image
    }
    
    func configure(url: String) {
        if let url = URL(string: url) {
            // Download image
        }
    }
    
}
