//
//  FoodCollectionViewCell.swift
//  Lobster
//
//  Created by JustDoIt on 11.09.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import UIKit

class FoodCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var generalImageView: UIImageView! {
        didSet {
            generalImageView.cornerRadius()
        }
    }
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var orderButton: UIButton! {
        didSet {
            orderButton.cornerRadius()
            orderButton.border(color: .redDefault, width: 1.0)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.cornerRadius()
        self.shadow()
    }

    func configure(with food: Dish!) {
        if let imageURL = food.imageLinks.first {
            setImageViewWith(url: imageURL)
        }
        self.titleLabel.text = food.title
        self.descriptionLabel.text = food.description
        self.priceLabel.text = LocalizeHelper.shared.convertDoubleToCurrency(double: food.price)
    }

    @IBAction func addToCheck(_ sender: Any) {

    }

    // MARK: - Private

    private func setImageViewWith(url: String) {
        let imageURL = URL(string: url)
        generalImageView.sd_setImage(with: imageURL, completed: nil)
    }

}
