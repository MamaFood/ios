//
//  InfoCollectionViewCell.swift
//  Lobster
//
//  Created by JustDoIt on 19.01.2019.
//  Copyright © 2019 RGS. All rights reserved.
//

import UIKit

class InfoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var containLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.borderColor = UIColor.grayDefault.cgColor
        self.layer.borderWidth = 1.0
    }
    
    func configure(title: String, contain: String) {
        titleLabel.text = title
        containLabel.text = contain
    }
}
