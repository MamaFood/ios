//
//  ReviewButtonCollectionViewCell.swift
//  Lobster
//
//  Created by JustDoIt on 25.01.2019.
//  Copyright © 2019 RGS. All rights reserved.
//

import UIKit

class ReviewButtonCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var informationLabel: UILabel!
    @IBOutlet weak var buttonImageView: UIImageView!
    
    func configure(text: String, image: UIImage!) {
        informationLabel.text = text
        if let image = image {
            buttonImageView.image = image
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.border()
    }
    
}
