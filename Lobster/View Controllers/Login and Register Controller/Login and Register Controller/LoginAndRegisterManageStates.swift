//
//  LoginViewControllerStates.swift
//  Lobster
//
//  Created by JustDoIt on 09.08.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import UIKit

extension LoginAndRegisterViewController {

    func changeState(state: LoginViewStates) {
        if self.isViewLoaded {
            switch state {
            case .signUp:
                activateSignUpView()
            case .signIn:
                activateSignInView()
            }
        }
        currentState = state
    }

    private func activateSignUpView() {
        UIView.transition(with: whiteView, duration: 0.5, options: .transitionFlipFromLeft, animations: {
            self.centerLabel.text = localize("SIGN UP")

            let haveAccountAttrubutedString = NSAttributedString(string: localize("Have account?"), attributes: [.foregroundColor: UIColor.black])
            let signInAttrubutedString = NSAttributedString(string: "Sign In", attributes: [.foregroundColor: UIColor.blue])
            let changeStateButtonAttributedString = NSMutableAttributedString()
            changeStateButtonAttributedString.append(haveAccountAttrubutedString)
            changeStateButtonAttributedString.append(NSAttributedString(string: " "))
            changeStateButtonAttributedString.append(signInAttrubutedString)

            self.changeStateButton.setAttributedTitle(changeStateButtonAttributedString, for: .normal)
            self.loginButton.setTitle(localize("Sign Up"), for: .normal)
            self.forgetPasswordButton.isHidden = true
        }, completion: nil)
    }

    private func activateSignInView() {
        UIView.transition(with: whiteView, duration: 0.5, options: .transitionFlipFromRight, animations: {
            self.centerLabel.text = localize("SIGN IN")

            let dontHaveAccountAttrubutedString = NSAttributedString(string: localize("Don't have an account?"), attributes: [.foregroundColor: UIColor.black])
            let signUpAttrubutedString = NSAttributedString(string: "Sign Up", attributes: [.foregroundColor: UIColor.blue])
            let changeStateButtonAttributedString = NSMutableAttributedString()
            changeStateButtonAttributedString.append(dontHaveAccountAttrubutedString)
            changeStateButtonAttributedString.append(NSAttributedString(string: " "))
            changeStateButtonAttributedString.append(signUpAttrubutedString)

            self.changeStateButton.setAttributedTitle(changeStateButtonAttributedString, for: .normal)
            self.loginButton.setTitle(localize("Sign In"), for: .normal)
            self.forgetPasswordButton.isHidden = false
        }, completion: nil)
    }

    func activateWrongEmailAlert() {
        self.emailTextFieldLine.backgroundColor = UIColor.redDefault
    }

    func activateWrongPassAlert() {
        self.passwordTextFieldLine.backgroundColor = UIColor.redDefault
    }

    func resetWrongAndPassAlerts() {
        self.emailTextFieldLine.backgroundColor = UIColor.grayDefault
        self.passwordTextFieldLine.backgroundColor = UIColor.grayDefault
    }

}
