//
//  LoginViewController.swift
//  Lobster
//
//  Created by JustDoIt on 07.08.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import UIKit
import GoogleSignIn

enum LoginViewStates {
    case signUp, signIn
}

class LoginAndRegisterViewController: UIViewController, GIDSignInUIDelegate {

    var currentState: LoginViewStates!

    @IBOutlet weak var whiteView: UIView!
    @IBOutlet weak var centerLabel: UILabel!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var emailTextFieldLine: UIView!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var passwordTextFieldLine: UIView!
    @IBOutlet weak var forgetPasswordButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var changeStateButton: UIButton!

    // MARK: Life cycle
    override func loadView() {
        super.loadView()

        emailTextField.delegate = self
        passwordTextField.delegate = self

        if currentState == nil {
            currentState = .signIn
        }

        // Keyboard Hide When Tapping Empty View
        let viewTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(viewTap)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        changeState(state: currentState)
    }

    @objc private func dismissKeyboard() {
        view.endEditing(true)
    }

}

extension LoginAndRegisterViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        resetWrongAndPassAlerts()
    }
}
