//
//  RegisterViewControllerActions.swift
//  Lobster
//
//  Created by JustDoIt on 16.08.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import UIKit

extension LoginAndRegisterViewController {

    @IBAction func changeStateButtonPressed(_ sender: Any) {
        switch currentState {
        case .signUp?:
            changeState(state: .signIn)
        case .signIn?:
            changeState(state: .signUp)
        default:
            changeState(state: .signUp)
        }
        resetValidationWarnings()
        resetWrongAndPassAlerts()
    }

    @IBAction func signInOrSignUpPressed(_ sender: Any) {
        if currentState == LoginViewStates.signIn {
            login(email: emailTextField.text!, password: passwordTextField.text!)
        } else if (currentState == LoginViewStates.signUp) {
            if validateRegisterFields() == true {
                if let registerImageViewController = Coordinator.instantiateViewController(with: .registerImageSetViewController) as? RegisterImageSetViewController {
                 registerImageViewController.email = self.emailTextField.text
                 registerImageViewController.password = self.passwordTextField.text
                 Coordinator.shared.presentViewController(viewController: registerImageViewController, modalTransitionStyle: .crossDissolve, completion: nil)
                }
//                Navigator.shared.presentViewController(identifier: .registerImageSetViewController, completion: nil)
                /*let continueRegisterViewController = Navigator.instantiateViewController(with: .continueRegisterViewController) as! ContinueRegisterViewController
                continueRegisterViewController.email = self.emailTextField.text
                continueRegisterViewController.password = self.passwordTextField.text
                Navigator.shared.presentViewController(viewController: continueRegisterViewController, modalTransitionStyle: .crossDissolve, completion: nil)*/
            }
        }
    }

    func login(email: String, password: String) {
        API.shared.login(email: email, password: password) { response in
            if response == true {
                Coordinator.shared.setRootViewControllerWith(identifier: .tabBarController, modalTransitionStyle: nil)
            } else {
                self.activateWrongEmailAlert()
                self.activateWrongPassAlert()
                let alertController = UIAlertController.init(title: "Wrong!", message: "Wrong email or password", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: { (_) in
                    self.view.endEditing(true)
                }))
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }

    func resetValidationWarnings() {
        emailTextField.layer.borderWidth = 0.0
        passwordTextField.layer.borderWidth = 0.0
    }

}
