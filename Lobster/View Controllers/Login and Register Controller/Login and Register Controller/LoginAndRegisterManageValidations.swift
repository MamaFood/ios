//
//  LoginAndRegisterManageValidations.swift
//  Lobster
//
//  Created by JustDoIt on 22.08.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import Foundation

extension LoginAndRegisterViewController {

    func validateRegisterFields() -> Bool {
        if (validateLoginEmail() == false) {
            return false
        }
        if (validateLoginPassword() == false) {
            return false
        }
        return true
    }

    private func validateLoginEmail() -> Bool {
        if let email = emailTextField.text {
            if email.isValidEmail() {
                emailTextField.layer.borderWidth = 0.0
                return true
            } else {
                emailTextField.layer.borderWidth = 1.0
                return false
            }
        }
        return false
    }

    private func validateLoginPassword() -> Bool {
        if let password = passwordTextField.text {
            if password.isValidPassword() {
                passwordTextField.layer.borderWidth = 0.0
                return true
            } else {
                passwordTextField.layer.borderWidth = 1.0
                return false
            }
        }
        return false
    }

}
