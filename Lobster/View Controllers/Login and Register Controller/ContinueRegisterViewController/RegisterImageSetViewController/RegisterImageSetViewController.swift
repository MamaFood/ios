//
//  RegisterImageSetViewController.swift
//  Lobster
//
//  Created by JustDoIt on 17.11.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import UIKit

class RegisterImageSetViewController: UIViewController {

    var email: String!
    var password: String!

    private var imagePicker = UIImagePickerController()

    @IBOutlet weak var avatarImageView: UIImageView!

    @IBAction func takePhotoFromCameraPressed(_ sender: Any) {
        imagePicker.sourceType = .camera
        imagePicker.cameraDevice = .front
        imagePicker.delegate = self
        present(imagePicker, animated: true, completion: nil)
    }

    @IBAction func usePhotoFromGalleryPressed(_ sender: Any) {
        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate = self
        present(imagePicker, animated: true, completion: nil)
    }

    @IBAction func nextButtonPressed(_ sender: Any) {
        if let continueRegisterViewController = Coordinator.instantiateViewController(with: .continueRegisterViewController) as? ContinueRegisterViewController {
         continueRegisterViewController.email = email
         continueRegisterViewController.password = password
         Coordinator.shared.presentViewController(viewController: continueRegisterViewController, modalTransitionStyle: .coverVertical, completion: nil)
        }
    }

    @IBAction func backButtonPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}

extension RegisterImageSetViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            avatarImageView.image = image
        }
        avatarImageView.layer.masksToBounds = true
        avatarImageView.layer.cornerRadius = avatarImageView.frame.height/2
        avatarImageView.contentMode = .scaleAspectFill
        self.dismiss(animated: true, completion: nil)
    }

}
