//
//   ContinueRegisterManageActions.swift
//  Lobster
//
//  Created by JustDoIt on 23.08.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import UIKit

extension ContinueRegisterViewController {

    @IBAction func backButtonPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func nextButtonPressed(_ sender: Any) {
        guard validateRegisterFields() == true else { return }

        let phoneNumber = phoneNumberTextField.text?.components(separatedBy: .whitespaces).joined()
        let params = ["fullName": fullNameTextField.text, "phone": phoneNumber, "email": email, "password": password]
        API.shared.request(.createAccount(body: params)) { (_, error) in
            guard error != nil else { return }
            API.shared.login(email: self.email, password: self.password, completion: { (isSuccess) in
                if isSuccess {
                    Coordinator.shared.setRootViewControllerWith(identifier: .tabBarController, modalTransitionStyle: .crossDissolve)
                } else {
                    let alertController = UIAlertController(title: "Error", message: "Try again later", preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: { _ in
                        self.view.endEditing(true)
                    }))
                    self.present(alertController, animated: true, completion: nil)
                }
            })
        }
    }

}
