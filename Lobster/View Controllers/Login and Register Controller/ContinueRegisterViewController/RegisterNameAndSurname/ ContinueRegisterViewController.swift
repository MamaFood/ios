//
//   ContinueRegisterViewController.swift
//  Lobster
//
//  Created by JustDoIt on 20.08.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import UIKit
import PhoneNumberKit

class  ContinueRegisterViewController: UIViewController {

    var email: String!
    var password: String!

    @IBOutlet weak var whiteView: UIView!
    @IBOutlet weak var fullNameTextField: UITextField!
    @IBOutlet weak var phoneNumberTextField: PhoneNumberTextField!

    @IBOutlet weak var whiteViewTopConstraint: NSLayoutConstraint!

    override func loadView() {
        super.loadView()

        // Keyboard Hide When Tapping Empty View
        let viewTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(viewTap)

//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: NSNotification.Name.UIResponder.keyboardWillShowNotification, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: NSNotification.Name.UIResponder.keyboardWillHideNotification, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)

    }

    @objc private func dismissKeyboard() {
        view.endEditing(true)
    }

    @objc func keyboardWillShow(notification: NSNotification) {
        self.whiteViewTopConstraint.constant = 50
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        self.whiteViewTopConstraint.constant = 180
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
        }
    }

}
