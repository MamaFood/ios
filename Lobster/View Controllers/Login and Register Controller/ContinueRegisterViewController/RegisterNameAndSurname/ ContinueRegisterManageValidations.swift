//
//   ContinueRegisterManageValidations.swift
//  Lobster
//
//  Created by JustDoIt on 22.08.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import Foundation

extension ContinueRegisterViewController {

    func validateRegisterFields() -> Bool {
        if (validateLoginName() == false) {
            return false
        }
        if (phoneNumberTextField.isValidNumber == false) {
            return false
        }
        return true
    }

    private func validateLoginName() -> Bool {
        if let fullName = fullNameTextField.text {
            if fullName.isValidName() {
                fullNameTextField.layer.borderWidth = 0.0
                return true
            } else {
                fullNameTextField.layer.borderWidth = 1.0
                return false
            }
        }
        return false
    }

}
