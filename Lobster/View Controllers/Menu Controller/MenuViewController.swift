//
//  MenuViewController.swift
//  Lobster
//
//  Created by JustDoIt on 04.12.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import UIKit

struct MenuViewControllerControl {
    var image: UIImage?
    var title: String!
    var viewControllerIdentifier: ViewControllerIdentifier!
}

class MenuViewController: UIViewController {

    let kMenuControlCellIdentifier = "menuControlCellIdentifier"

    @IBOutlet weak var avatarBackgroundView: UIView!
    @IBOutlet weak var avatarImageView: UIImageView!

    var menuControllers: [MenuViewControllerControl] = [
    MenuViewControllerControl(image: UIImage(named: "menu-user"),
                              title: "Profile",
                              viewControllerIdentifier: .launchScreenViewController),
    MenuViewControllerControl(image: UIImage(named: "menu-cart"),
                              title: "Cart",
                              viewControllerIdentifier: .searchViewController),
    MenuViewControllerControl(image: UIImage(named: "menu-cards"),
                              title: "Card",
                              viewControllerIdentifier: .scannerViewController),
    MenuViewControllerControl(image: UIImage(named: "menu-bag"),
                              title: "Checkout",
                              viewControllerIdentifier: .cartViewController),
    MenuViewControllerControl(image: UIImage(named: "menu-user"),
                              title: "Sign Out",
                              viewControllerIdentifier: .signInViewController)
    ]

    override func loadView() {
        super.loadView()
        configureViews()
//        createRandomMenuControllers(15)
    }

    func configureViews() {
        if let backGrndView = avatarBackgroundView {
            backGrndView.clipsToBounds = false
            backGrndView.layer.shadowOpacity = 0.5
            backGrndView.layer.shadowOffset = CGSize(width: 0, height: 2)
            backGrndView.layer.shadowRadius = 3
            backGrndView.layer.shadowColor = UIColor.white.cgColor
            backGrndView.layer.shadowPath = UIBezierPath(roundedRect: self.avatarImageView.frame,
                                                         cornerRadius: self.avatarImageView.layer.cornerRadius).cgPath
        }
    }

    func createRandomMenuControllers(_ count: Int32) {
        for _ in 0..<count {
            let menuController = MenuViewControllerControl(image: UIImage(named: "user"),
                                                           title: "Profile",
                                                           viewControllerIdentifier: .searchViewController)
            menuControllers.append(menuController)
        }
    }

}

extension MenuViewController: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kMenuControlCellIdentifier,
                                                         for: indexPath) as? MenuViewCollectionViewCell {
            cell.configureWith(self.menuControllers[indexPath.row])
            return cell
        }
        return UICollectionViewCell()
    }

    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return menuControllers.count
    }

}

extension MenuViewController: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        Coordinator.shared.presentViewController(identifier: menuControllers[indexPath.row].viewControllerIdentifier,
                                               completion: nil)
    }

}
