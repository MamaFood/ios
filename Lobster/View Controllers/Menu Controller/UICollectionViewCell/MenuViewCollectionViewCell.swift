//
//  MenuViewCollectionViewCell.swift
//  Lobster
//
//  Created by JustDoIt on 05.12.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import UIKit

class MenuViewCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var controlTitle: UILabel!
    @IBOutlet weak var controlImageView: UIImageView!

    func configureWith(_ model: MenuViewControllerControl) {
        self.controlTitle.text = model.title
        self.controlImageView.image = model.image
    }

}
