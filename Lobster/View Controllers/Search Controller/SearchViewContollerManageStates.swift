//
//  SearchViewContollerManageStates.swift
//  Lobster
//
//  Created by JustDoIt on 26.11.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import UIKit

enum SearchViewControllerState {
    case searchRestaurants, searchPlace
}

extension SearchViewController {

    func changeStateTo(state: SearchViewControllerState) {
        switch state {
        case .searchRestaurants: do {
            searchBarBackViewTopToSuperViewConstraint.priority = .defaultLow
            searchBarBackViewTopToBackgroundViewConstraint.priority = .defaultHigh
            searchBar.showsCancelButton = false
            yourLocationLabel.font = UIFont.regular(size: 10.0)
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
            searchModelPlaces = nil
            self.searchCollectionView.reloadData()
            self.searchBar.text = ""
            }
        case .searchPlace: do {
            searchBarBackViewTopToSuperViewConstraint.priority = .defaultHigh
            searchBarBackViewTopToBackgroundViewConstraint.priority = .defaultLow
            yourLocationLabel.font = UIFont.medium(size: 17.0)
            searchBar.showsCancelButton = true
            searchBar.becomeFirstResponder()
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
            searchModelPlaces = nil
            self.searchCollectionView.reloadData()
            self.searchBar.text = ""
            }
        }
        self.state = state
    }

}
