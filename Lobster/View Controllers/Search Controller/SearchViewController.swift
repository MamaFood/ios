//
//  SearchViewController.swift
//  Lobster
//
//  Created by JustDoIt on 05.11.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {

    @IBOutlet weak var yourLocationLabel: UILabel!
    @IBOutlet weak var locationButton: UIButton! {
        didSet {
//            locationButton.titleLabel?.minimumScaleFactor = 0.5
//            locationButton.titleLabel?.numberOfLines = 0
//            locationButton.titleLabel?.adjustsFontSizeToFitWidth = true
        }
    }
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var searchBarBackView: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var searchCollectionView: UICollectionView!
    @IBOutlet weak var emptyCollectionViewImageView: UIImageView!

    var state: SearchViewControllerState = .searchRestaurants

    @IBOutlet weak var searchBarBackViewTopToBackgroundViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchBarBackViewTopToSuperViewConstraint: NSLayoutConstraint!

    var restaurants: [Restaurant] = [Restaurant]()

    var searchModelPlaces: [SearchPlacesModel]?

    override func loadView() {
        super.loadView()

        searchBar.delegate = self

        let viewTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        viewTapGestureRecognizer.cancelsTouchesInView = false
        self.view.addGestureRecognizer(viewTapGestureRecognizer)

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }

    @objc func viewTapped() {
        self.view.endEditing(true)
    }

    @IBAction func placeButtonPressed(_ sender: Any) {
        changeStateTo(state: .searchPlace)
    }

    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 && searchBarBackView.frame.origin.x >= keyboardSize.height + 10 {
                self.view.frame.origin.y -= keyboardSize.height / 2
            }
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0 {
                self.view.frame.origin.y += keyboardSize.height / 2
            }
        }
    }

}

extension SearchViewController: UISearchBarDelegate {

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if state == .searchRestaurants {
            restaurants.append(TempRestaurant.sharedInstance.getRestaurant())
            searchCollectionView.reloadData()
        } else {
            searchPlaces(text: searchText) { (response, _) -> Void in
                self.searchModelPlaces = response
                self.searchCollectionView.reloadData()
            }
        }
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        changeStateTo(state: .searchRestaurants)
        searchModelPlaces = nil
        self.searchCollectionView.reloadData()
        self.searchBar.text = ""
        viewTapped()
    }

}

extension SearchViewController: UICollectionViewDataSource, UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if state == .searchRestaurants {
            if restaurants.count <= 0 {
                emptyCollectionViewImageView.isHidden = false
            } else {
                emptyCollectionViewImageView.isHidden = true
            }
            return restaurants.count
        } else {
            guard let places = searchModelPlaces else { return 0 }
            if places.count <= 0 {
                emptyCollectionViewImageView.isHidden = false
            } else {
                emptyCollectionViewImageView.isHidden = true
            }
            return places.count
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if state == .searchRestaurants {
            if let searchRestaurantCell = collectionView.dequeueReusableCell(withReuseIdentifier: "searchRestaurantCell", for: indexPath) as? SearchRestaurantCollectionViewCell {
            searchRestaurantCell.configure(with: restaurants[indexPath.row])
            return searchRestaurantCell
            }
        } else {
            if let searchPlaceCell = collectionView.dequeueReusableCell(withReuseIdentifier: "searchPlaceCell", for: indexPath) as? SearchPlaceCollectionViewCell {
                if let place = searchModelPlaces?[indexPath.row] {
                    searchPlaceCell.configureWith(place: place)
                }
            return searchPlaceCell
            }
        }
        return UICollectionViewCell()
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if state == .searchRestaurants {

        } else {
            guard let place = searchModelPlaces?[indexPath.row] else { return }
            changeStateTo(state: .searchRestaurants)
            self.searchBar.text = ""
            let googlePlacesString = place.addres
            self.locationButton.setTitle(googlePlacesString, for: .normal)
            searchModelPlaces = nil
            self.searchCollectionView.reloadData()
            viewTapped()
        }
    }

}

extension SearchViewController: UIScrollViewDelegate {

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        viewTapped()
    }

}
