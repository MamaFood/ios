//
//  SearchViewControllerManageActions.swift
//  Lobster
//
//  Created by JustDoIt on 02.12.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import Foundation
import GooglePlaces

extension SearchViewController {

    func searchPlaces(text: String, completion: @escaping ([SearchPlacesModel]?, Error?) -> Void) {
        GMSPlacesClient.shared().autocompleteQuery(text, bounds: nil, boundsMode: .bias, filter: nil) { (response, error) in
            var places: [SearchPlacesModel] = [SearchPlacesModel]()
            for place in response! {
                let searchPlacesTemp = SearchPlacesModel.init(addres: place.attributedFullText.string, additionalAddres: place.attributedPrimaryText.string)
                places.append(searchPlacesTemp)
            }
            if let err = error {
                completion(nil, err)
            } else {
                completion(places, nil)
            }
        }
    }

}
