//
//  SearchRestaurantCollectionViewCell.swift
//  Lobster
//
//  Created by JustDoIt on 07.11.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import UIKit

//searchRestaurantCell

class SearchRestaurantCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var averageCheckLabel: UILabel!

    // Rating Stars
    @IBOutlet weak var ratingStarFirst: UIImageView!
    @IBOutlet weak var ratingStarSecond: UIImageView!
    @IBOutlet weak var ratingStarThird: UIImageView!
    @IBOutlet weak var ratingStarFourth: UIImageView!
    @IBOutlet weak var ratingStarFifth: UIImageView!

    func configure(with restaurant: Restaurant) {
        titleLabel.text = restaurant.name
        typeLabel.text = restaurant.type
        averageCheckLabel.text = "\(restaurant.averageCheck ?? 0.0)"
    }
}
