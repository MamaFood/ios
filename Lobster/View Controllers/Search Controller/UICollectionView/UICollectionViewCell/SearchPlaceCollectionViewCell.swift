//
//  SearchPlaceCollectionViewCell.swift
//  Lobster
//
//  Created by JustDoIt on 26.11.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import UIKit

class SearchPlaceCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!

    func configureWith(place: SearchPlacesModel) {
        titleLabel.text = place.addres
        typeLabel.text = place.additionalAddres
    }

}
