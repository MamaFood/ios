//
//  CategoriesViewController.swift
//  Lobster
//
//  Created by JustDoIt on 26.10.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import UIKit
import GooglePlaces

class CategoriesViewController: UIViewController {
    
    private let kGeneralCollectionViewCell = "categoriesCollectionViewCell"
    private let kRestaurantCollectionViewCell = "restaurantCollectionViewCell"
    private let kTypeCollectionViewCell = "typeCollectionViewCell"
    private let kOfferCollectionViewCell = "offerCollectionViewCell"
    private let kOfferImageViewCollectionViewCell = "offerImageViewCollectionViewCell"
    
    let kNavigationViewBarHideHeight: CGFloat = CGFloat(exactly: 55.0)!
    let kNavigationViewBarShowHeight: CGFloat = CGFloat(exactly: 227.0)!
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var generalCollectionView: UICollectionView!
    
    var backgroundView: UIView!
    
    var allRestaurantTypes: [String] = ["Азербайджан",
                                        "Одесские закуски",
                                        "Кальяная",
                                        "Мороженое",
                                        "Fast&Food"]
    let restaurantCategoriesTitles: [String] = ["Top 10 Restaurants",
                                                "Newly Opened Restaurants",
                                                "Top 10 Restaurants"]
    var restaurantsCollection: [Restaurant] = TempRestaurant.sharedInstance.getRestaurants(count: 10)
    var selectedRestaurantTypes: [String] = [String]()
    
    override func loadView() {
        super.loadView()
        
        backgroundView = UIView(frame: generalCollectionView.frame)
        backgroundView.backgroundColor = .black
        backgroundView.alpha = 0.5
        backgroundView.isHidden = true
        //        self.view.addSubview(backgroundView)
        
        let viewTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        viewTapGestureRecognizer.cancelsTouchesInView = false
        //        self.view.addGestureRecognizer(viewTapGestureRecognizer)
        
        generalCollectionView.register(UINib(nibName: "CategoriesCollectionViewCell",
                                             bundle: Bundle.main),
                                       forCellWithReuseIdentifier: kGeneralCollectionViewCell)
        generalCollectionView.register(UINib(nibName: "CategoriesOfferCollectionViewCell",
                                             bundle: Bundle.main),
                                       forCellWithReuseIdentifier: kOfferCollectionViewCell)
        
        // Fetch from API
        fetchRestauarnts(params: ["": ""])
        fetchRestaurantTypes()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let navigationBarView = NavigationBarView.instanceFromNib()
        navigationBarView.delegate = self
        navigationBarView.translatesAutoresizingMaskIntoConstraints = false
        
        self.headerView.addSubview(navigationBarView)
        
        self.headerView.addConstraint(NSLayoutConstraint.init(item: navigationBarView,
                                                              attribute: .height,
                                                              relatedBy: .equal,
                                                              toItem: self.headerView,
                                                              attribute: .height,
                                                              multiplier: 1.0,
                                                              constant: 0.0))
        self.headerView.addConstraint(NSLayoutConstraint.init(item: navigationBarView,
                                                              attribute: .width,
                                                              relatedBy: .equal,
                                                              toItem: self.headerView,
                                                              attribute: .width,
                                                              multiplier: 1.0,
                                                              constant: 0.0))
        self.headerView.addConstraint(NSLayoutConstraint.init(item: navigationBarView,
                                                              attribute: .top,
                                                              relatedBy: .equal,
                                                              toItem: self.headerView,
                                                              attribute: .top,
                                                              multiplier: 1.0,
                                                              constant: 0.0))
        self.headerView.addConstraint(NSLayoutConstraint.init(item: navigationBarView,
                                                              attribute: .centerX,
                                                              relatedBy: .equal,
                                                              toItem: self.headerView,
                                                              attribute: .centerX,
                                                              multiplier: 1.0,
                                                              constant: 0.0))
        
    }
    
    @objc func viewTapped() {
        self.view.endEditing(true)
    }
    
}

extension CategoriesViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView,
                        didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag == 2 {
            if let generalCollectionViewTypeCell = collectionView.cellForItem(
                at: indexPath) as? CategoriesTypeCollectionViewCell {
                addCellToVisible(typeCollectionViewCell: generalCollectionViewTypeCell)
            }
        } else if collectionView.tag == 3 {
            if let restaurantViewController = Coordinator.instantiateViewController(
                with: .restaurantViewController) as? RestaurantViewController {
                restaurantViewController.restaurant = restaurantsCollection[indexPath.row]
                Coordinator.shared.presentViewController(
                    viewController: restaurantViewController,
                    modalTransitionStyle: .crossDissolve,
                    completion: nil)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == generalCollectionView || collectionView.tag == 1 {
            return 3
        } else if collectionView.tag == 2 {
            return allRestaurantTypes.count
        } else if collectionView.tag == 3 {
            return restaurantsCollection.count
        } else if collectionView.tag == 4 {
            return 3
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == generalCollectionView {
            // Offers and Discounts
            if indexPath.row == 0 {
                if let offerCollectionViewCell = collectionView.dequeueReusableCell(
                    withReuseIdentifier: kOfferCollectionViewCell,
                    for: indexPath) as? CategoriesOfferCollectionViewCell {
                    offerCollectionViewCell.configure(with: self)
                    return offerCollectionViewCell
                }
            } else {
                // Restaurant Categories
                if let restaurantCategoriesCollectionViewCell = collectionView.dequeueReusableCell(
                    withReuseIdentifier: kGeneralCollectionViewCell,
                    for: indexPath) as? CategoriesCollectionViewCell {
                    var restaurantCategoriesCellState = CategoriesCollectionViewCellState.normal
                    if indexPath.row == 1 {
                        restaurantCategoriesCellState = CategoriesCollectionViewCellState.hiddenTypesCollectionViewCell
                    }
                    restaurantCategoriesCollectionViewCell.configureWithViewController(
                        viewController: self,
                        categoriesTitle: restaurantCategoriesTitles[indexPath.row],
                        state: restaurantCategoriesCellState)
                    return restaurantCategoriesCollectionViewCell
                }
            }
        } else if collectionView.tag == 2 {
            // Categories Type
            if let generalCollectionViewTypeCell = collectionView.dequeueReusableCell(
                withReuseIdentifier: kTypeCollectionViewCell,
                for: indexPath) as? CategoriesTypeCollectionViewCell {
                generalCollectionViewTypeCell.configureWith(type: allRestaurantTypes[indexPath.row])
                checkCellToVisible(typeCollectionViewCell: generalCollectionViewTypeCell)
                return generalCollectionViewTypeCell
            }
        } else if collectionView.tag == 3 {
            // Restaurant
            if let generalCollectionViewRestaurantCell = collectionView.dequeueReusableCell(
                withReuseIdentifier: kRestaurantCollectionViewCell,
                for: indexPath) as? CategoriesRestaurantCollectionViewCell {
                generalCollectionViewRestaurantCell.configureWith(
                    restaurant: restaurantsCollection[indexPath.row])
                return generalCollectionViewRestaurantCell
            }
        } else if collectionView.tag == 4 {
            // Offers Image
            if let categoriesOfferImageViewCollectionViewCell = collectionView.dequeueReusableCell(
                withReuseIdentifier: kOfferImageViewCollectionViewCell,
                for: indexPath) as? CategoriesOfferImageViewCollectionViewCell {
                categoriesOfferImageViewCollectionViewCell.configure(with: #imageLiteral(resourceName: "offer"))
                return categoriesOfferImageViewCollectionViewCell
            }
        }
        return UICollectionViewCell()
    }
    
    func checkCellToVisible(typeCollectionViewCell: CategoriesTypeCollectionViewCell) {
        // Check Cell to be visible or not
        if selectedRestaurantTypes.contains(typeCollectionViewCell.category) {
            typeCollectionViewCell.setVisible(isVisible: true)
        } else {
            typeCollectionViewCell.setVisible(isVisible: false)
        }
    }
    
    func addCellToVisible(typeCollectionViewCell: CategoriesTypeCollectionViewCell) {
        // Check Cell to be visible or not
        if selectedRestaurantTypes.contains(typeCollectionViewCell.category) {
            typeCollectionViewCell.setVisible(isVisible: false)
            selectedRestaurantTypes.removeAll { ($0 == typeCollectionViewCell.category) }
        } else {
            typeCollectionViewCell.setVisible(isVisible: true)
            selectedRestaurantTypes.append(typeCollectionViewCell.category)
        }
    }
    
}

extension CategoriesViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else {
            return CGSize.zero
        }
        let screenWidth = UIScreen.main.bounds.width
        if collectionView == generalCollectionView {
            if indexPath.row == 0 {
                let itemWidth = screenWidth
                let itemHeight = itemWidth * 1.3
                return CGSize(width: itemWidth, height: itemHeight)
            } else if indexPath.row == 1 {
                // FIXME: Check height from API
                let itemHeight = layout.itemSize.height - 50.0
                return CGSize(width: screenWidth, height: itemHeight)
            } else {
                return CGSize(width: screenWidth, height: layout.itemSize.height)
            }
        } else if collectionView.tag == 4 {
            let itemHeight = layout.itemSize.height * 1.4 - 60
            return CGSize(width: itemHeight / 1.3, height: itemHeight)
        } else {
            return layout.itemSize
        }
    }
    
}

extension CategoriesViewController: NavigationBarViewDelegate {
    func mapButtonPressed() {
        Coordinator.shared.presentViewController(identifier: .mapViewController, completion: nil)
    }
    
    func searchButtonPressed() {
        Coordinator.shared.presentViewController(identifier: .searchViewController, completion: nil)
    }
    
    func menuButtonPressed() {
        Coordinator.shared.presentViewController(identifier: .menuViewController, completion: nil)
    }
    
    func searchCollectionViewDidShow() {
        self.view.bringSubviewToFront(headerView)
        headerViewHeightConstraint.constant = kNavigationViewBarShowHeight
        UIView.animate(withDuration: kAnimateTime) {
            self.view.layoutIfNeeded()
        }
    }
    
    func searchCollectionViewDidHide() {
        headerViewHeightConstraint.constant = kNavigationViewBarHideHeight
        UIView.animate(withDuration: kAnimateTime) {
            self.view.layoutIfNeeded()
        }
    }
    
    func willHideNavigationBarView() {
        self.view.endEditing(true)
    }
    
}
