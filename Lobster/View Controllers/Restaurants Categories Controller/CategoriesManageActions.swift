//
//  CategoriesManageActions.swift
//  Lobster
//
//  Created by JustDoIt on 02.12.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import Foundation
import GooglePlaces

extension CategoriesViewController {
    
    func reloadCollectionView() {
        for cell in self.generalCollectionView.visibleCells {
            if let cell = cell as? CategoriesCollectionViewCell {
                cell.typesCollectionView.reloadData()
                cell.restaurantCollectionView.reloadData()
            }
        }
    }

    func fetchRestaurantTypes() {
        DataManager.shared.getRestaurantTypes { (types, error) in
            guard error == nil, types != nil else { return }
            if let types = types as? [String] {
                self.allRestaurantTypes = types
            }

            self.reloadCollectionView()
        }
    }

    func fetchRestauarnts(params: [String: Any]) {
        DataManager.shared.getRestaurant(params: params) { (restaurants, error) in
            guard error == nil, restaurants != nil else { return }
            if let restaurants = restaurants as? [Restaurant] {
                self.restaurantsCollection = restaurants
            }

            self.reloadCollectionView()
        }
    }

}
