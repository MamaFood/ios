//
//  RestaurantCategoriesCollectionViewCell.swift
//  Lobster
//
//  Created by JustDoIt on 27.10.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import UIKit

enum CategoriesCollectionViewCellState {
    case normal, hiddenTypesCollectionViewCell
}

class CategoriesCollectionViewCell: UICollectionViewCell {

    private let kRestaurantCollectionViewCell = "restaurantCollectionViewCell"
    private let kTypeCollectionViewCell = "typeCollectionViewCell"

    @IBOutlet weak var typesCollectionViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var categoriesTitleLabel: UILabel!
    @IBOutlet weak var typesCollectionView: UICollectionView!
    @IBOutlet weak var restaurantCollectionView: UICollectionView!
    
    private func hideTypeCollectionView() {
        typesCollectionViewHeightConstraint.constant = 0.0
        self.layoutIfNeeded()
    }

    func configureWithViewController(viewController: UIViewController,
                                     categoriesTitle: String,
                                     state: CategoriesCollectionViewCellState) {
        guard viewController is UICollectionViewDelegate || viewController is UICollectionViewDataSource else {
            return
        }
        self.categoriesTitleLabel.text = categoriesTitle

        self.restaurantCollectionView.register(UINib(nibName: "CategoriesRestaurantCollectionViewCell",
                                                     bundle: Bundle.main),
                                               forCellWithReuseIdentifier: kRestaurantCollectionViewCell)
        self.typesCollectionView.register(UINib(nibName: "CategoriesTypeCollectionViewCell",
                                                bundle: Bundle.main),
                                          forCellWithReuseIdentifier: kTypeCollectionViewCell)
        if let viewController = viewController as? UICollectionViewDataSource {
            self.typesCollectionView.dataSource = viewController
            self.restaurantCollectionView.dataSource = viewController
        }
        if let viewController = viewController as? UICollectionViewDelegate {
            self.typesCollectionView.delegate = viewController
            self.restaurantCollectionView.delegate = viewController
        }
        if state == .hiddenTypesCollectionViewCell {
            hideTypeCollectionView()
        }
    }

}
