//
//  CategoriesCollectionViewTypeCell.swift
//  Lobster
//
//  Created by JustDoIt on 04.11.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import UIKit

class CategoriesTypeCollectionViewCell: UICollectionViewCell {

    var category: String!

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backgrndView: UIView! {
        didSet {
            backgrndView.layer.borderWidth = 1.25
            backgrndView.cornerRadius()
        }
    }

    private var isPressed: Bool = false

    func configureWith(type: String) {
        self.category = type
        titleLabel.text = type
    }

    func setVisible(isVisible: Bool) {
        if isVisible == true {
            backgrndView.layer.borderColor = UIColor.redDefault.cgColor
            titleLabel.textColor = UIColor.redDefault
        } else {
            backgrndView.layer.borderColor = UIColor.grayDefault.cgColor
            titleLabel.textColor = .black
        }
    }

}
