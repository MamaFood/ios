//
//  CategoriesCollectionViewRestaurantCell.swift
//  Lobster
//
//  Created by JustDoIt on 04.11.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import UIKit
import Alamofire

class CategoriesRestaurantCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var backView: UIView! {
        didSet {
            backView.cornerRadius()
//            backView.shadow()
        }
    }
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var reviewsCountLabel: UILabel!
    @IBOutlet weak var averageCheckLabel: UILabel!
    @IBOutlet weak var imageBackgroundView: UIView! {
        didSet {
            imageBackgroundView.shadow()
        }
    }
    @IBOutlet weak var imageView: UIImageView! {
        didSet {
            imageView.cornerRadius(radius: 10.0)
        }
    }
    @IBOutlet weak var lineView: UIView! {
        didSet {
//            lineView.backgroundColor = UIColor.grayDefault
        }
    }

    func configureWith(restaurant: Restaurant) {
        titleLabel.text = restaurant.name
        descriptionLabel.text = "Odessa, Fontanskaya"
        ratingLabel.text = "5"
        reviewsCountLabel.text = "14K"
        averageCheckLabel.text = "$ \(restaurant.averageCheck ?? 0.0)"

        guard let imageLink = restaurant.imageLinks.first else {
            return
        }

        Alamofire.request(imageLink).responseData { (responseData) in
            self.imageView.image = UIImage(data: responseData.data ?? Data())
        }

        /*let config = URLSessionConfiguration.default
        let manager = SessionManager(configuration: config)
        
        manager.request(imageLinks[0], method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).validate().responseData { (data) in
            self.imageView.image = UIImage(data: data.data ?? Data())
        }*/
    }

}
