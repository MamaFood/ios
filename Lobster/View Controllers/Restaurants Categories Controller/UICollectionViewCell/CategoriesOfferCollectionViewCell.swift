//
//  CategoriesOfferCollectionViewCell.swift
//  Lobster
//
//  Created by JustDoIt on 16.12.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import UIKit

class CategoriesOfferCollectionViewCell: UICollectionViewCell, UIScrollViewDelegate {
    
    let kOfferImageViewCollectionViewCell = "offerImageViewCollectionViewCell"

    @IBOutlet weak var offersCollectionView: UICollectionView! {
        didSet {
            offersCollectionView.register(UINib(nibName: "CategoriesOfferImageViewCollectionViewCell",
                                                bundle: Bundle.main),
                                          forCellWithReuseIdentifier: kOfferImageViewCollectionViewCell)
        }
    }
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!

    func configure(with viewController: UIViewController!) {
        titleLabel.text = "Exlusive Offers"
        descriptionLabel.text = "Discount Coupons & Cashback"
        if let viewController = viewController as? UICollectionViewDataSource {
            offersCollectionView.dataSource = viewController
        }
        if let viewController = viewController as? UICollectionViewDelegate {
            offersCollectionView.delegate = viewController
        }
    }

}
