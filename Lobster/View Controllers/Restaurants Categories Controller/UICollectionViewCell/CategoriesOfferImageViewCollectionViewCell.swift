//
//  CategoriesOfferImageViewCollectionViewCell.swift
//  Lobster
//
//  Created by JustDoIt on 05.01.2019.
//  Copyright © 2019 RGS. All rights reserved.
//

import UIKit

class CategoriesOfferImageViewCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var offerImageView: UIImageView! {
        didSet {
            offerImageView.cornerRadius()
        }
    }
    
    func configure(with image: UIImage!) {
        if let image = image {
            self.offerImageView.image = image
        }
    }
}
