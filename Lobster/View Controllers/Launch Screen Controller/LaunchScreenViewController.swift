//
//  LaunchScreenViewController.swift
//  Lobster
//
//  Created by JustDoIt on 14.10.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import UIKit

class LaunchScreenViewController: UIViewController {

    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var pageControl: UIPageControl!

    let logoImageViewArray = ["startup-background-1", "startup-background-2", "startup-background-3"]
    let titleLabelArray = ["Lorem ipsum dolor.", "Lorem sit amet.", "Ipsum dolor sit ."]
    let descriptionLabelArray = ["Lorem ipsum dolor sit amet, consectetur do eiusmod tempor incididunt et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur do eiusmod tempor incididunt et dolore magna aliqua.", "Lorem sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.", "Lorem ipsum dolor sit amet, sed do eiusmod magna aliqua."]

    override func loadView() {
        super.loadView()
        let rightSwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(rightSwipe))
        let leftSwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(leftSwipe))
        rightSwipeGestureRecognizer.direction = .right
        leftSwipeGestureRecognizer.direction = .left

        self.view.addGestureRecognizer(rightSwipeGestureRecognizer)
        self.view.addGestureRecognizer(leftSwipeGestureRecognizer)
        pageControlValueChanged(pageControl)
    }

    @IBAction func getStartedButtonPressed(_ sender: Any) {
        Coordinator.shared.setRootViewControllerWith(identifier: .signUpViewController, modalTransitionStyle: .crossDissolve)
    }

    @IBAction func signInButtonPressed(_ sender: Any) {
        Coordinator.shared.setRootViewControllerWith(identifier: .signInViewController, modalTransitionStyle: .crossDissolve)
    }

    @IBAction func pageControlValueChanged(_ sender: Any) {
        setLogoTitleAndDescription(index: pageControl.currentPage)
    }

    func setLogoTitleAndDescription(index: Int) {
        if let image = UIImage(named: logoImageViewArray[index]) {
            UIView.transition(with: logoImageView,
                              duration: kAnimateTime,
                              options: .transitionCrossDissolve,
                              animations: { self.logoImageView.image = image },
                              completion: nil)
        }
        UIView.transition(with: titleLabel,
                          duration: 0.4,
                          options: .transitionCrossDissolve,
                          animations: { self.titleLabel.text = self.titleLabelArray[index] }, completion: nil)
        UIView.transition(with: descriptionLabel,
                          duration: 0.4,
                          options: .transitionCrossDissolve,
                          animations: { self.descriptionLabel.text = self.descriptionLabelArray[index] }, completion: nil)
    }

    @objc func rightSwipe() {
        if pageControl.currentPage > 0 {
            pageControl.currentPage -= 1
        }
        pageControlValueChanged(pageControl)
    }

    @objc func leftSwipe() {
        if pageControl.currentPage < pageControl.numberOfPages {
            pageControl.currentPage += 1
        }
        pageControlValueChanged(pageControl)
    }

}
