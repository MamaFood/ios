//
//  Categories.swift
//  Lobster
//
//  Created by JustDoIt on 23.11.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import Foundation

class Category {

    var id: Int!
    var title: String!

    init (id: Int, title: String) {
        self.id = id
        self.title = title
    }

}
