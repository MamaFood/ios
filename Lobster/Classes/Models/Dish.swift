//
//  Food.swift
//  Lobster
//
//  Created by JustDoIt on 11.09.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import Foundation

struct Dish: Decodable {
    var id: Int
    var title: String!
    var imageLinks: [String]!
    var type: String!
    var price: Double!
    var description: String!
    var available: Bool
    var deleted: Bool

    init(id: Int, title: String, imageLinks: [String], type: String, price: Double, description: String, available: Bool, deleted: Bool) {
        self.id = id
        self.title = title
        self.imageLinks = imageLinks
        self.type = type
        self.price = price
        self.description = description
        self.available = available
        self.deleted = deleted
    }
}
