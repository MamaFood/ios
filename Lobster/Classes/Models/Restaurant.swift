//
//  Restaurant.swift
//  Lobster
//
//  Created by JustDoIt on 25.08.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import Foundation

struct Restaurant: Decodable {
    var id: Int
    var name: String!
    var type: String!
    var menuID: String!
    var averageCheck: Double!
    var description: String!
    var lat: Double!
    var lng: Double!
    var phone1: String!
    var phone2: String!
    var phone3: String!
    var phone4: String!
    var phone5: String!
    var facebook: String!
    var instagram: String!
    var googlePlus: String!
    var twitter: String!
    var imageLinks: [String]!
    var liked: Bool!
    var rating: Float!
    var time: String!

    init(id: Int, name: String, type: String, menuID: String, averageCheck: Double, description: String, lat: Double, lng: Double, phone1: String, phone2: String, phone3: String, phone4: String, phone5: String, facebook: String, instagram: String, googlePlus: String, twitter: String, imageLinks: [String], liked: Bool, rating: Float, time: String) {
        self.id = id
        self.name = name
        self.type = type
        self.menuID = menuID
        self.averageCheck = averageCheck
        self.description = description
        self.lat = lat
        self.lng = lng
        self.phone1 = phone1
        self.phone2 = phone2
        self.phone3 = phone3
        self.phone4 = phone4
        self.phone5 = phone5
        self.facebook = facebook
        self.instagram = instagram
        self.googlePlus = googlePlus
        self.twitter = twitter
        self.imageLinks = imageLinks
        self.liked = liked
        self.rating = rating
        self.time = time
    }
}
