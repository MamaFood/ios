//
//  LBError.swift
//  Roomster
//
//  Created by Igor Tudoran on 03.04.2018.
//  Copyright © 2018 Roomster. All rights reserved.
//

import Foundation

// Error codes:
//
// -1000 - Network unknown error
// -1001 - Database save error
// -1002 - Login with \(message)

protocol LBErrorProtocol: LocalizedError {
    var code: Int { get }
    var message: String { get set }
    var title: String { get }
    var data: Data? { get }
}

struct LBError: LBErrorProtocol {

    var title: String = localize("Error")
    var code: Int
    var message: String
    var data: Data?

    init(message: String, code: Int, data: Data? = nil) {
        self.message = message
        self.code = code
        self.data = data
    }
}
