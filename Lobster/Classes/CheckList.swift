//
//  CheckList.swift
//  Lobster
//
//  Created by JustDoIt on 05.10.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import Foundation

class CheckList {

    var dishes: [Dish]!
    var summ: Double!

    func addDish(dish: Dish) {
        dishes.append(dish)
        summ += dish.price
    }

    func removeDish(dish: Dish) {

    }

    func removeDishByIndex(index: Int) {
        dishes.remove(at: index)
    }

}
