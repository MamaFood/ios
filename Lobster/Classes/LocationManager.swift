//
//  LocationManager.swift
//  Lobster
//
//  Created by JustDoIt on 25.09.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import Foundation
import CoreLocation

extension FloatingPoint {
    var degreesToRadians: Self { return self * .pi / 180 }
    var radiansToDegrees: Self { return self * 180 / .pi }
}

class LocationManager {

    static let shared = LocationManager()

    private let locationManager = CLLocationManager()

    private init() {
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
    }

    func distanceToLocationBy(lat: CLLocationDegrees, lng: CLLocationDegrees) -> Double {
        if let userCoordinate = locationManager.location {
            let distance = userCoordinate.distance(from: CLLocation(latitude: lng, longitude: lat))
            let distanceInMetres = distance/1000
            return (distanceInMetres*100).rounded()/100
        }
        return 0
    }

}
