//
//  TempMenu.swift
//  Lobster
//
//  Created by JustDoIt on 11.11.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import Foundation

class TempMenu {

    static let sharedInstance = TempMenu()

    func getRandomName(length: Int) -> String {
        let letters = "     abcdefghijklmnopqrstuvwxyz"
        return String((0...length-1).map { _ in letters.randomElement()! })
    }

    func getRandomNumber(length: Int) -> Double {
        return Double.random(in: 0...400)
    }

    func getDish() -> Dish {
        let dish = Dish(id: 0,
                        title: getRandomName(length: 8),
                        imageLinks: ["https://food.fnr.sndimg.com/content/dam/images/food/fullset/2018/6/0/FN_snapchat_coachella_wingman%20.jpeg.rend.hgtvcom.616.462.suffix/1523633513292.jpeg"],
                        type: "FOOD",
                        price: getRandomNumber(length: 3),
                        description: getRandomName(length: 150),
                        available: true,
                        deleted: false)
        return dish
    }

    func getDishes(count: Int) -> [Dish] {
        var dishesArray: [Dish] = [Dish]()
        for _ in 0...count {
            dishesArray.append(getDish())
        }
        return dishesArray
    }

}
