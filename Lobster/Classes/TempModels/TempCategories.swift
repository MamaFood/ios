//
//  TempCategories.swift
//  Lobster
//
//  Created by JustDoIt on 23.11.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import Foundation

class TempCategory {

    static let sharedInstance = TempCategory()

    func getRandomName(length: Int) -> String {
        let letters = "     abcdefghijklmnopqrstuvwxyz"
        return String((0...length-1).map { _ in letters.randomElement()! })
    }

    func getRandomNumber(length: Int) -> Int {
        return Int.random(in: 0...400)
    }

    func getCategory() -> Category {
        let category = Category(id: getRandomNumber(length: 6), title: getRandomName(length: 9))
        return category
    }

    func getCategories(count: Int) -> [Category] {
        var categoriesArray = [Category]()
        for _ in 0...count {
            let category = getCategory()
            categoriesArray.append(category)
        }
        return categoriesArray
    }

}
