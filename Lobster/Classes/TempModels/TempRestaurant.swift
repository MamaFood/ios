//
//  TempRestaurant.swift
//  Lobster
//
//  Created by JustDoIt on 04.11.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import Foundation

class TempRestaurant {

    static let sharedInstance = TempRestaurant()

    func getRandomName(length: Int) -> String {
        let letters = " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0...length-1).map { _ in letters.randomElement()! })
    }

    func getRestaurant() -> Restaurant {
        let restaurant = Restaurant(id: 0,
                                    name: getRandomName(length: 8),
                                    type: "SeaFood",
                                    menuID: "2132",
                                    averageCheck: 132.0,
                                    description: getRandomName(length: 40),
                                    lat: 46.434074,
                                    lng: 30.747500,
                                    phone1: "+380676617717",
                                    phone2: "+380676617717",
                                    phone3: "+380676617717",
                                    phone4: "+380676617717",
                                    phone5: "+380676617717",
                                    facebook: "http://facebook.com",
                                    instagram: "http://instagram.com",
                                    googlePlus: "http://google.com",
                                    twitter: "http://twitter.com",
                                    imageLinks: ["http://apsheron.od.ua/userfls/slider//large/25_banketnyy.jpg"],
                                    liked: false,
                                    rating: 4.4,
                                    time: "11am - 9pm")
        return restaurant
    }

    func getRestaurants(count: Int) -> [Restaurant] {
        var restaurantArray: [Restaurant] = [Restaurant]()
        for _ in 0...count {
            restaurantArray.append(getRestaurant())
        }
        return restaurantArray
    }

}
