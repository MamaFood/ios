//
//  LocalizeHelper.swift
//  Lobster
//
//  Created by JustDoIt on 16.08.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import Foundation

class LocalizeHelper {

    static let shared = LocalizeHelper()

    var locale: Locale!

    private init() {
        locale = Locale.current
    }

    func convertDoubleToCurrency(double: Double!) -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .currency
        numberFormatter.locale = locale

        return numberFormatter.string(from: NSNumber(value: double))!
    }

    static func localizedString(forKey key: String) -> String {
        return Bundle.main.localizedString(forKey: key, value: "", table: nil)
    }

}
