//
//  DataManager.swift
//  Roomster
//
//  Created by Igor Tudoran on 29.03.2018.
//  Copyright © 2018 Roomster. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class DataManager {
    static let shared = DataManager()

    typealias DataResponse = ((Decodable?, LBError?) -> Void)
}

extension DataManager {

    func getRestaurant(params: Parameters!, completion: @escaping DataResponse) {
        API.shared.request(.restaurants(body: params)) { (response, error) in
            if (error != nil) {
                completion(nil, error)
            }
            do {
                let restaurantList = try JSONDecoder().decode([Restaurant].self, from: response.rawData())
                completion(restaurantList, nil)
            } catch {
                completion(nil, error as? LBError)
            }
        }
    }

    func getRestaurantTypes(completion: @escaping DataResponse) {
        API.shared.request(.restaurantsTypes()) { (response, error) in
            if (error != nil) {
                completion(nil, error)
            }
            do {
                let restaurantTypesList = try JSONDecoder().decode([String].self, from: response.rawData())
                completion(restaurantTypesList, nil)
            } catch {
                completion(nil, error as? LBError)
            }
        }
    }

    func getDishes(params: Parameters, completion: @escaping DataResponse) {
        API.shared.request(.dishes(body: params)) { (response, error) in
            if (error != nil) {
                completion(nil, error)
            }
            do {
                let dishesList = try JSONDecoder().decode([Dish].self, from: response.rawData())
                completion(dishesList, nil)
            } catch {
                completion(nil, error as? LBError)
            }
        }
    }

}
