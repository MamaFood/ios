//
//  DataBase.swift
//  Roomster
//
//  Created by Igor Tudoran on 29.03.2018.
//  Copyright © 2018 Roomster. All rights reserved.
//

import Foundation
import SwiftyJSON

struct DataBase {
    static let shared = DataBase()
}
