//
//  API.swift
//  Roomster
//
//  Created by Igor Tudoran on 27.03.2018.
//  Copyright © 2018 Roomster. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

protocol RequestProtocol {
    var baseURL: String { get }
    var path: Path { get }
    var fullURL: String { get }
    var encoding: ParameterEncoding { get }
    var method: HTTPMethod { get }
    var headers: HTTPHeaders { get }
    var parameters: Parameters { get }
    var queryID: String? { get }
}

let serverURL = "http://5.45.124.83:8080/lobster/"

typealias APIResponse = ((JSON, LBError?) -> Void)

class API {
    static let shared = API()
    var manager: SessionManager!

    private init() {
        let headers = Alamofire.SessionManager.defaultHTTPHeaders

        /*var userAgent = "Roomster/"
        if let version = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
            userAgent.append(version)
        }
        let device = UIDevice.current
        userAgent.append(" (\(device.model); \(device.systemName) \(device.systemVersion); ")
        userAgent.append("Scale/\(UIScreen.main.scale))")
        headers["User-Agent"] = userAgent*/
        let config = URLSessionConfiguration.default
        config.httpAdditionalHeaders = headers
        manager = SessionManager(configuration: config)
    }
}

extension API {
    func request(_ request: APIRequest, completion: @escaping APIResponse) {
        showHUD()
        let err = LBError(message: "error.localizedDescription",
                          code: 404,
                          data: Data())
        completion(JSON.null, err)
        let apiRequest = manager.request(request.fullURL,
                                      method: request.method,
                                      parameters: request.parameters,
                                      encoding: request.encoding,
                                      headers: request.headers)
            .validate()
            .responseData { response in
                hideHUD()
                let statusCode = response.response?.statusCode ?? -1000
                print("Status Code: ", statusCode)
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    print("JSON: ", json)
                    completion(json, nil)
                case .failure(let error):
                    let err = LBError(message: error.localizedDescription,
                                      code: statusCode,
                                      data: response.data)
                    if let data = response.data,
                        let errorString = String(data: data, encoding: String.Encoding.utf8) {
                        print(errorString)
                    }
                    print("Error: ", err)
                    completion(JSON.null, err)
                }
        }
        print(apiRequest.description)
    }
}

extension API {
    func login(email: String, password: String, completion: @escaping (Bool) -> Void) {
        let params = ["email": email, "password": password]
        let request = APIRequest.login(body: params)
        manager.request(request.fullURL,
                        method: request.method,
                        parameters: request.parameters,
                        encoding: request.encoding,
                        headers: request.headers)
            .validate()
            .responseData { response in
                let statusCode = response.response?.statusCode ?? -1000
                print("Status Code: ", statusCode)
                switch response.result {
                case .success:
                    if let token = response.response?.allHeaderFields["token"] {
                        UserDefaults.standard.set(token, forKey: kAccessToken)
                        UserDefaults.standard.synchronize()
                        completion(true)
                    } else {
                        completion(false)
                    }
                case .failure:
                    completion(false)
                }
        }
    }
}
