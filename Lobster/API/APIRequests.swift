//
//  APIRequests.swift
//  Lobster
//
//  Created by JustDoIt on 06.12.2018.
//  Copyright © 2018 RGS. All rights reserved.
//

import Foundation
import Alamofire

enum Path: String {
    // User
    case me = "users/me"
    case createAccount = "users/register"
    case updateEmail = "users/me/email"

    // Login
    case login = "login"

    // Restaurant
    case restaurants = "restaurants"
    case restaurantsTypes = "restaurants/types"

    // Dish
    case dishes = "dishes"

    // Orders
    case orders = "orders"
    case ordersByRestaurant = "orders/restaurant/"
}

enum APIRequest {
    // User
    case me()
    case createAccount(body: Parameters)
    case login(body: Parameters)

    // Restaurants
    case restaurants()
    case restaurants(body: Parameters)
    case restaurantsTypes()

    // Dish
    case dishes(body: Parameters)

    // Orders
    case getOrders()
    case createOrder(body: Parameters)
    case updateOrder(body: Parameters)
    case getOrdersByID(body: Parameters)
    case rejectOrderByID(body: Parameters)
    case getOrdersByRestaurantID(body: Parameters)
}

extension RequestProtocol {

    var baseURL: String {
        return serverURL
    }

    var fullURL: String {
        let url = baseURL + path.rawValue
        return url
    }

}

extension APIRequest: RequestProtocol {

    var path: Path {
        switch self {
        case .createAccount:
            return .createAccount
        case .me:
            return .me
        case .login:
            return .login
        case .restaurants:
            return .restaurants
        case .dishes:
            return .dishes
        case .getOrders,
             .createOrder,
             .updateOrder,
             .getOrdersByID,
             .rejectOrderByID:
            return .orders
        case .getOrdersByRestaurantID:
            return .ordersByRestaurant
        case .restaurantsTypes:
            return .restaurantsTypes
        }
    }

    var method: HTTPMethod {
        switch self {
        case .createAccount,
             .login,
             .createOrder:
            return .post
        case .me,
             .restaurants,
             .dishes,
             .getOrders,
             .getOrdersByID,
             .getOrdersByRestaurantID,
             .restaurantsTypes:
            return .get
        case .updateOrder:
            return .put
        case .rejectOrderByID:
            return .delete
        }
    }

    var encoding: ParameterEncoding {
        switch self {
        case .createAccount,
             .login:
            return JSONEncoding.default
        default:
            return URLEncoding.default
        }
    }

    var parameters: Parameters {
        var parameters: Parameters
        switch self {
        case .createAccount(let body),
             .login(let body),
             .restaurants(let body),
             .dishes(let body):
            parameters = body
        default:
            parameters = Parameters()
        }
        print("parameters: \(parameters)")
        return parameters
    }

    var queryID: String? {
        return nil
    }

    var headers: HTTPHeaders {
        var headers = HTTPHeaders()
        switch self {
        case .me,
             .restaurants,
             .restaurantsTypes,
             .dishes: do {
                if let accessToken = UserDefaults.standard.value(forKey: kAccessToken) as? String {
                    headers["Authorization"] = accessToken
                }
            }
        default:
            break
        }
        return headers
    }
}
